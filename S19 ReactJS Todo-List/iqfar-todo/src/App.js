import React from 'react';
import './App.css';
import Header from './component/Header';
import TodoList from './component/TodoComponent/TodoList';
//import TodoBar from './component/TodoComponent/TodoBar';

function App() {
  return (
    <div className="App">
      <Header />
      <TodoList />
    </div>
  );
}

export default App;

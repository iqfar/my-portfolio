import React, { Component } from 'react';
import TodoItem from './TodoItem';

export default class TodoBar extends Component{
    render(){
        return this.props.data.map((d) =>(
            <TodoItem key={d._id} d = {d} markComplete={this.props.markComplete} delete={this.props.delete} edit={this.props.edit}/>
        ))
    }
}
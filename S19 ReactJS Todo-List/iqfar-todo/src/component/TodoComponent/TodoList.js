import React, { Component } from 'react';
import axios from 'axios';
import TodoBar from './TodoBar';
import Grid from '@material-ui/core/Grid'
import CreateTodo from './createTodo';
import './style.css';


class TodoList extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: [],
        }
    }

    getData = () =>{
        axios.get(API_URL)
        .then(res => {
            const data = res.data
            this.setState({ 
                data: data.results,
            })
        })
    }

    componentDidMount(){
        this.getData();
        setInterval(this.getData.bind(this), 100);
    }

    delete = (_id) => {
        axios.delete(`${API_URL}${_id}`)
    }


   create = (title) =>{
       axios.post(API_URL, {
           title: title,
           isComplete: false
       })
   }

   edit = (_id, title) =>{
       axios.put(`${API_URL}${_id}`, {
           title: title
       })
   }

   markComplete = (_id) => {
       this.setState({
           data: this.state.data.map(d => {
               if(d._id === _id)
                d.isComplete = !d.isComplete;
                return d;
           })
       })
   }




    render(){
        return(
            <div>
                <div id='input'>
                    <CreateTodo create={this.create} />
                </div>
                <div id='list'>
                    <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="flex-start"
                    >
                    <TodoBar data={this.state.data} markComplete={this.markComplete} delete={this.delete} edit={this.edit} />
                    </Grid>
                </div>
            </div>
        )
    }


}

const API_URL = 'https://btm-rn.herokuapp.com/api/v1/todo/';

export default TodoList;
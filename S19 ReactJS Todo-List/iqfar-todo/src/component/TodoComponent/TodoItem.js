import React, { Component } from 'react';
import { Checkbox, ButtonGroup, Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import './style.css';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';

class TodoItem extends Component{
    constructor(props){
        super(props);
        this.state = {
            checked: false,
            isEdited: false,
            title: ''
        }
    }

    handleChange(){
        if(!this.state.checked){
            this.setState({
                checked: true
            });
        }else{
            this.setState({
                checked: false
            })
        }
    }

    editChange(event){
        event.preventDefault();
        this.setState({ isEdited: true });
    }

    cancelEditChange(){
        this.setState({ isEdited: false })
    }

    saveEditChange(_id){
        axios.put(`https://btm-rn.herokuapp.com/api/v1/todo/${_id}`, {
            title: this.state.title
        })
    }

    onEditChange = (event) =>{
        this.setState({
            title: event.target.value
        })
    }

    markComplete(_id){
        if(!this.state.checked){
            axios.put(`https://btm-rn.herokuapp.com/api/v1/todo/${_id}`, {
            isComplete: true
        })
        }else if(this.state.checked){
            axios.put(`https://btm-rn.herokuapp.com/api/v1/todo/${_id}`, {
            isComplete: false
        })
        }
    }

    render(){
        const {_id, title} = this.props.d;
        let status;
        if(this.state.checked){
            status = (
                <p className='txt'><del>{title}</del></p>
            )
        }else{
            status = (
                <p className='txt'>{title}</p>
            )
        }
        
        const edit = (
            <div>
                <TextField required id="standard-required" label="Required" type='text' placeholder={title} onChange={this.onEditChange} value={this.state.title}/>
                <ButtonGroup id='main-btn'>
                    <Button className='btn-act' onClick={() => {
                this.props.edit(this.props.d._id, this.state.title)
                this.setState({
                    isEdited: false,
                }) 
            }} > <SaveIcon/> </Button>
                    <Button className='btn-act' onClick={() => this.cancelEditChange()} > <CancelIcon /> </Button>
                </ButtonGroup>
            </div>
        )

        return(
            <div className='todo-bar'>
                <Checkbox className='check' 
                    checked={this.state.checked}
                    onClick={(event) => this.handleChange(event)}
                    onChange={this.markComplete.bind(this, _id)}
                />
                {status}
                <ButtonGroup id='main-btn'>
                    <Button className='btn-act' onClick={(event) => this.editChange(event)}> <EditIcon /> </Button>
                    <Button className='btn-act' onClick={this.props.delete.bind(this, _id)}> <DeleteIcon /> </Button>
                </ButtonGroup>
                {this.state.isEdited && edit}
            </div>
        )
    }
}

export default TodoItem;
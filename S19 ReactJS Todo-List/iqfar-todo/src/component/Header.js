import React, { Component } from 'react';
import './stylesheet.css';

class Header extends Component{
    render(){
       return(
        <header>
                <h1>Todo List App</h1>
        </header>
       )
    }
}

export default Header;
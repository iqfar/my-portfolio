import React, {useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Image, Button, Icon} from 'react-native-elements';
import Logo from '../../asset/logo.png';
import {
  TextInput,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import {API_LOGIN, API_USER} from '../../API';
import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import {useDispatch} from 'react-redux';
import {ISLOGIN} from '../../redux/Reducer/case';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';

const Login = () => {
  const navigation = useNavigation();
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [alert, setAlert] = useState(false);
  const [loading, setLoading] = useState(false);
  const [hidden, setHidden] = useState(true);
  const dispatch = useDispatch();

  const onChangeEmail = val => {
    setEmail(val);
  };

  const onChangePassword = val => {
    setPassword(val);
  };

  const handleLogin = async () => {
    setLoading(true);
    try {
      const res = await Axios.post(API_LOGIN, {
        email: email,
        password: password,
      });

      if (res !== null) {
        const status = res.data.status;
        const data = res.data.data;
        if (status == 'success') {
          await AsyncStorage.setItem('userToken', data.token);
          setLoading(false);
          dispatch({type: ISLOGIN});
          navigation.navigate('Dashboard');
          navigation.reset({
            index: 0,
            routes: [{name: 'Dashboard'}],
          });
        } else {
          console.log('error');
          setLoading(false);
          setAlert(true);
        }
      } else {
        console.log('Login Failed');
        setAlert(true);
        setLoading(false);
      }
    } catch (error) {
      console.log(error, 'error');
      setLoading(false);
      setAlert(true);
    }
  };

  const googleLogin = async () => {
    try {
      GoogleSignin.configure({
        offlineAccess: true,
        webClientId:
          '647805002874-qubrb9pnoqoc8nirucv9mpt3dkbva8sf.apps.googleusercontent.com',
      });
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      if (userInfo !== null) {
        const token = await GoogleSignin.getTokens();
        try {
          const res = await Axios.post(`${API_USER}/googleLogin`, {
            name: userInfo.user.name,
            access_token: token.accessToken,
          });
          if (res !== null) {
            await AsyncStorage.setItem('userToken', res.data.data.token);
            navigation.navigate('Dashboard');
          }
        } catch (error) {
          console.log(error, 'error');
        }
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('error occured SIGN_IN_CANCELLED');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('error occured IN_PROGRESS');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('error occured PLAY_SERVICES_NOT_AVAILABLE');
      } else {
        console.log(error);
        console.log('error occured unknow error');
      }
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.logoContainer}>
          <Image source={Logo} style={styles.logo} />
          <Text style={styles.logoText}>PROTRA</Text>
        </View>
        <View style={styles.inputContainer}>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Email</Text>
            <TextInput style={styles.inputBox} onChangeText={onChangeEmail} />
          </View>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangePassword}
            />
            <Icon
              type="feather"
              name={hidden ? 'eye-off' : 'eye'}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title="Log In"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={handleLogin}
          />
          <Text style={styles.loginText}>or Login with</Text>
          <GoogleSigninButton
            onPress={googleLogin}
            style={[styles.button, {alignSelf: 'center'}]}
          />
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('Signup')}>
            <Text style={styles.signup}>Sign Up ></Text>
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
      <AwesomeAlert
        show={alert}
        showProgress={false}
        title="Oops"
        message="Wrong Email or Password"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="Oke"
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          setAlert(false);
        }}
      />
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
    justifyContent: 'flex-start',
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 80,
  },
  logo: {
    width: 15,
    height: 15,
    marginHorizontal: 5,
  },
  logoText: {
    fontFamily: 'Metropolis',
    fontWeight: 'bold',
    fontSize: 18,
    letterSpacing: -0.7,
  },
  inputContainer: {
    marginHorizontal: 16,
  },
  inputBar: {
    justifyContent: 'flex-start',
    marginVertical: 12,
  },
  inputText: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  inputBox: {
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    backgroundColor: '#FFF',
    marginTop: 8,
    fontSize: 16,
    fontFamily: 'Montserrat',
    paddingHorizontal: 14,
  },
  button: {
    marginVertical: 13,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  signup: {
    alignSelf: 'flex-end',
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 16,
  },
  buttonContainer: {
    marginVertical: 45,
    paddingBottom: 95,
  },
  icon: {
    alignSelf: 'flex-end',
    bottom: 37,
    right: 20,
  },
  loginText: {
    fontFamily: 'Montserrat',
    textAlign: 'center',
    marginTop: 3,
  },
});

import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  TextInput,
  TouchableWithoutFeedback,
  ScrollView,
  FlatList,
} from 'react-native-gesture-handler';
import {Image, Button} from 'react-native-elements';
import addImage from '../../asset/addImage.png';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';
import {createTask} from '../../redux/Action';

const CreateTask = () => {
  const [title, setTitle] = useState('');
  const [descrip, setDescrip] = useState('');
  const [image, setImage] = useState([]);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const loading = useSelector(state => state.task.isLoading);
  const idProject = useSelector(state => state.project.idProject);

  const onChangeTitle = val => {
    setTitle(val);
  };

  const onChangeDescrip = val => {
    setDescrip(val);
  };

  const handlePickImage = async () => {
    try {
      const images = await ImagePicker.openPicker({
        multiple: true,
        width: 100,
        height: 100,
      });
      console.log(images);
      setImage(images);
    } catch (error) {
      console.log(error, 'error');
    }
  };

  const handleCreateTask = () => {
    let dataForm = new FormData();
    dataForm.append('title', title);
    dataForm.append('description', descrip);
    if (image !== []) {
      image.forEach((pict, index) => {
        dataForm.append('image', {
          uri: pict.path,
          type: pict.mime,
          name: `picture[${index}]`,
        });
      });
    }
    dispatch(createTask(idProject, dataForm, navigation));
  };

  const renderItem = ({item}) => (
    <TouchableWithoutFeedback onPress={handlePickImage}>
      <Image source={{uri: item.path}} containerStyle={styles.image} />
    </TouchableWithoutFeedback>
  );

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Title</Text>
          <TextInput
            placeholder="Title"
            style={styles.titleInput}
            onChangeText={onChangeTitle}
          />
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.title}>Description</Text>
          <TextInput
            placeholder="Description"
            multiline={true}
            style={styles.descriptionInput}
            onChangeText={onChangeDescrip}
          />
        </View>
        <View style={styles.imageContainer}>
          <Text style={styles.title}>Task Image</Text>
          <View>
            <FlatList
              data={image}
              renderItem={renderItem}
              keyExtractor={item => item.path}
              horizontal={true}
            />
            <TouchableWithoutFeedback onPress={handlePickImage}>
              <Image source={addImage} containerStyle={styles.image} />
            </TouchableWithoutFeedback>
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title="Create Task"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={handleCreateTask}
          />
          <Button
            title="Cancel"
            containerStyle={styles.button}
            titleStyle={styles.cancelTitle}
            buttonStyle={styles.cancelButton}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ScrollView>
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default CreateTask;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  titleContainer: {
    marginHorizontal: 16,
    marginTop: 28,
  },
  title: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  titleInput: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginTop: 8,
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionInput: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginTop: 8,
    height: 100,
    textAlignVertical: 'top',
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  image: {
    width: 100,
    height: 100,
    marginTop: 8,
    marginHorizontal: 4,
  },
  imageContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  button: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
  buttonContainer: {
    marginTop: 50,
  },
  assignContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  inputTag: {
    backgroundColor: '#E5E5E5',
  },
  inputContainer: {
    marginTop: 8,
    alignSelf: 'flex-start',
  },
});

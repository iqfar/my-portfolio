import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {StyleSheet} from 'react-native';
import {Header, Icon, Image, Button} from 'react-native-elements';
import {TouchableWithoutFeedback, FlatList} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {GET_PROJECT_ID} from '../../../redux/Case Type/Project';
import {Menu} from 'react-native-paper';
import {deleteProject} from '../../../redux/Action/ProjectAction';

const MyLeftComponent = () => {
  return <Text style={styles.headerText}>Project</Text>;
};

const DashboardProject = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const project = useSelector(state => state.project.project);
  const [index, setIndex] = useState(0);

  const showMenu = id => {
    setIndex(id);
  };

  const hideMenu = () => {
    setIndex(0);
  };

  const handleDetailNavigation = id => {
    dispatch({type: GET_PROJECT_ID, idProject: id});
    navigation.navigate('Project Detail');
    setIndex(0);
  };

  const handleEditNavigation = id => {
    dispatch({type: GET_PROJECT_ID, idProject: id});
    navigation.navigate('Edit Project');
    setIndex(0);
  };

  const handleDelete = id => {
    dispatch(deleteProject(id));
    setIndex(0);
  };

  const renderItem = ({item}) => (
    <View style={styles.projectCardBar}>
      <TouchableWithoutFeedback onPress={() => handleDetailNavigation(item.id)}>
        <View style={styles.projectBodyContainer}>
          <Image source={{uri: item.image}} style={styles.projectImage} />
          <Text style={styles.projectCardTitle}>{item.title}</Text>
        </View>
      </TouchableWithoutFeedback>
      <Menu
        visible={index === item.id ? true : false}
        onDismiss={hideMenu}
        anchor={
          <Icon
            type="feather"
            name="more-vertical"
            size={24}
            containerStyle={styles.projectCardMore}
            onPress={() => showMenu(item.id)}
          />
        }>
        <Menu.Item
          title="View"
          onPress={() => handleDetailNavigation(item.id)}
        />
        <Menu.Item title="Edit" onPress={() => handleEditNavigation(item.id)} />
        <Menu.Item title="Delete" onPress={() => handleDelete(item.id)} />
      </Menu>
    </View>
  );

  return (
    <>
      <Header
        leftComponent={<MyLeftComponent />}
        backgroundColor="#FFF"
        containerStyle={styles.containerHeader}
      />
      <View style={styles.container}>
        <View style={styles.projectContainer}>
          <Text style={styles.projectTitle}>My Project</Text>
        </View>
        <View style={styles.projectCardContainer}>
          <FlatList
            data={project}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        </View>
        <View style={styles.projectButtonContainer}>
          <Button
            icon={<Icon type="feather" name="plus" size={35} color="#FFF" />}
            containerStyle={styles.buttonContainer}
            onPress={() => navigation.navigate('Create Project')}
          />
        </View>
      </View>
    </>
  );
};

export default DashboardProject;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  containerHeader: {
    paddingTop: 10,
    height: 60,
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
    color: '#000',
    width: 100,
  },
  projectTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 24,
  },
  projectContainer: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
  projectCardContainer: {
    marginHorizontal: 16,
    marginVertical: 10,
    paddingBottom: 75,
  },
  projectCardBar: {
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 3,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 4,
  },
  projectImage: {
    width: 62,
    height: 62,
  },
  projectCardTitle: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginHorizontal: 30,
  },
  projectCardMore: {
    marginHorizontal: 16,
  },
  buttonContainer: {
    borderRadius: 100,
    alignSelf: 'center',
  },
  projectButtonContainer: {
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'flex-end',
    bottom: 30,
    right: 30,
  },
  projectBodyContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

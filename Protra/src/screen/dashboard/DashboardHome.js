import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Header, Icon, Image} from 'react-native-elements';
import {TouchableNativeFeedback, FlatList} from 'react-native-gesture-handler';
import Avatar from '../../asset/avatar.png';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {getAllProject} from '../../redux/Action/ProjectAction';
import {GET_PROJECT_ID} from '../../redux/Case Type/Project';
import messaging from '@react-native-firebase/messaging';
import {getNews} from '../../redux/Action';
import moment from 'moment';

const MyLeftComponent = () => {
  return <Text style={styles.headerText}>Dashboard</Text>;
};

const DashboardHome = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const project = useSelector(state => state.project.project);
  const task = useSelector(state => state.project.total_task);
  const history = useSelector(state => state.showUser.history);

  useEffect(() => {
    dispatch(getAllProject());
  }, [dispatch, project]);

  useEffect(() => {
    dispatch(getNews());
  }, [dispatch]);

  const handleDetailNavigation = id => {
    dispatch({type: GET_PROJECT_ID, idProject: id});
    navigation.navigate('Project Detail');
  };

  const renderItem = ({item}) => (
    <View style={styles.projectBar}>
      <Text style={styles.projectBarTitle}>{item.title}</Text>
      <Text
        style={[styles.projectBarTitle, {marginRight: 4, color: '#7B7B7B'}]}>
        {item.type}
      </Text>
      <Text
        style={[styles.projectBarTitle, {marginRight: 4, color: '#7B7B7B'}]}>
        {item.action} at {moment(item.time).format('LLL')}
      </Text>
    </View>
  );

  return (
    <>
      <Header
        leftComponent={<MyLeftComponent />}
        backgroundColor="#FFF"
        containerStyle={styles.containerHeader}
      />
      <View style={styles.container}>
        <View style={styles.headerStatusContainer}>
          <View style={styles.headerStatus}>
            <Text style={styles.headerTextStatus}>{project.length}</Text>
            <Text style={styles.headerStatusTitle}>Total Project</Text>
          </View>
          <View style={styles.headerStatus}>
            <Text style={styles.headerTextStatus}>
              {task !== null ? task : 0}
            </Text>
            <Text style={styles.headerStatusTitle}>Total Task</Text>
          </View>
        </View>
        <View style={styles.projectContainer}>
          <Text style={styles.projectTitle}>Latest Project</Text>
          <View style={styles.projectBarContainer}>
            <FlatList
              data={history}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              maxToRenderPerBatch={10}
            />
          </View>
        </View>
      </View>
    </>
  );
};

export default DashboardHome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
    color: '#000',
    width: 100,
  },
  containerHeader: {
    paddingTop: 10,
    height: 60,
    justifyContent: 'center',
  },
  headerStatus: {
    borderWidth: 1,
    borderColor: '#A2D5F2',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#FFF',
    marginVertical: 4,
  },
  headerStatusContainer: {
    marginVertical: 30,
    marginHorizontal: 16,
  },
  headerTextStatus: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 36,
    marginRight: 20,
  },
  headerStatusTitle: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginLeft: 20,
  },
  projectTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 24,
  },
  projectContainer: {
    marginHorizontal: 16,
  },
  Image: {
    width: 40,
    height: 40,
    borderRadius: 100,
  },
  projectBarImageContainer: {
    flexDirection: 'row',
    marginHorizontal: 14,
    marginVertical: 12,
  },
  projectBar: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#E5E5E5',
    marginVertical: 4,
    backgroundColor: '#FFF',
    padding: 8,
  },
  projectBarTitle: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginHorizontal: 16,
  },
  projectBarContainer: {
    marginBottom: 10,
    paddingBottom: 300,
  },
});

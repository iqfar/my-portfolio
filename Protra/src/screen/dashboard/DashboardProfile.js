import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Header, Image, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useSelector, useDispatch} from 'react-redux';
import {ScrollView} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import {ISLOGOUT} from '../../redux/Reducer/case';
import {GoogleSignin} from '@react-native-community/google-signin';

const MyLeftComponent = () => {
  return <Text style={styles.headerText}>Profile</Text>;
};

const DashboardProfile = () => {
  const profile = useSelector(state => state.showUser);
  const username = profile.name;
  const biodata = profile.bio;
  const email = profile.email;
  const image = profile.imageProfile;
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handleLogout = async () => {
    try {
      AsyncStorage.clear();
      GoogleSignin.signOut();
      dispatch({type: ISLOGOUT});
      navigation.navigate('Landing Page');
      navigation.reset({
        index: 0,
        routes: [{name: 'Landing Page'}],
      });
    } catch (error) {
      console.log(error, 'error');
    }
  };

  return (
    <>
      <Header
        leftComponent={<MyLeftComponent />}
        backgroundColor="#FFF"
        containerStyle={styles.containerHeader}
      />
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.imageContainer}>
            <Image source={{uri: image}} style={styles.imageProfile} />
          </View>
          <View style={styles.usernameContainer}>
            <Text style={styles.username}>{username}</Text>
          </View>
          <View style={styles.profileContainer}>
            <View style={styles.emailContainer}>
              <Text style={styles.profileTitle}>Email</Text>
              <Text style={styles.email}>{email}</Text>
            </View>
            <View style={styles.biodataContainer}>
              <Text style={styles.profileTitle}>Bio</Text>
              <Text style={styles.biodata}>{biodata}</Text>
            </View>
          </View>
          <View style={styles.editProfileContainer}>
            <Button
              title="Edit Profile"
              containerStyle={styles.button}
              ViewComponent={LinearGradient}
              linearGradientProps={LinearConfig}
              titleStyle={styles.buttonTitle}
              onPress={() => navigation.navigate('Edit Profile')}
            />
            <Button
              title="Log out"
              containerStyle={styles.button}
              titleStyle={styles.cancelTitle}
              buttonStyle={styles.cancelButton}
              onPress={handleLogout}
            />
          </View>
        </ScrollView>
      </View>
    </>
  );
};

export default DashboardProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  containerHeader: {
    paddingTop: 10,
    height: 60,
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
    color: '#000',
    width: 100,
  },
  imageProfile: {
    width: 105,
    height: 105,
    borderWidth: 2,
    borderColor: '#1589CB',
    borderRadius: 100,
  },
  imageContainer: {
    alignSelf: 'center',
    marginVertical: 25,
  },
  username: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 24,
  },
  usernameContainer: {
    marginHorizontal: 16,
    marginBottom: 24,
  },
  profileTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  email: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginVertical: 8,
  },
  emailContainer: {
    marginHorizontal: 16,
    marginVertical: 8,
  },
  biodataContainer: {
    marginHorizontal: 16,
    marginVertical: 8,
  },
  biodata: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginVertical: 8,
  },
  button: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
});

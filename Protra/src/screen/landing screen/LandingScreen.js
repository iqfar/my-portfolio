import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Image, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useNavigation} from '@react-navigation/native';
import Swiper from 'react-native-swiper';
import Starter from '../../asset/starter.png';
import Easy from '../../asset/easy.png';
import Team from '../../asset/team.png';
import Progress from '../../asset/progress.png';

const LandingScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Swiper containerStyle={styles.swiperContainer} loop={false}>
        <View style={styles.slide}>
          <Text style={styles.headerText}>Protra</Text>
          <Text style={styles.bodyText}>Upgrade your teamwork with Protra</Text>
          <Image source={Starter} style={styles.slide1Image} />
        </View>
        <View style={styles.slide}>
          <Text style={styles.headerText}>Easy to Use</Text>
          <Text style={styles.bodyText}>Simple and Easy</Text>
          <Image source={Easy} style={[styles.slide1Image, {height: 350}]} />
        </View>
        <View style={styles.slide}>
          <Text style={styles.headerText}>Collaborating with Team</Text>
          <Text style={styles.bodyText}>Work with your Team via online</Text>
          <Image source={Team} style={styles.slide1Image} />
        </View>
        <View style={styles.slide}>
          <Text style={styles.headerText}>Lifetime Report</Text>
          <Text style={styles.bodyText}>
            Track your team progress real time
          </Text>
          <Image source={Progress} style={styles.slide1Image} />
        </View>
      </Swiper>
      <View style={styles.buttonContainer}>
        <Button
          title="Log In"
          containerStyle={styles.button}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          titleStyle={styles.buttonTitle}
          onPress={() => navigation.navigate('Login')}
        />
        <Button
          title="Sign Up"
          containerStyle={[
            styles.button,
            {
              backgroundColor: '#F4F4F4',
              borderWidth: 1,
              borderColor: '#1589CB',
            },
          ]}
          titleStyle={[styles.buttonTitle, {color: '#1589CB'}]}
          type="outline"
          onPress={() => navigation.navigate('Signup')}
        />
      </View>
      <View style={{margin: 8}}>
        <Text style={{textAlign: 'center'}}>
          <Text>By creating an account, you agree to our</Text>
          <Text style={{fontWeight: 'bold'}}> Terms of Service</Text>
          <Text> and</Text>
          <Text style={{fontWeight: 'bold'}}> Privacy Policy</Text>.
        </Text>
      </View>
    </View>
  );
};

export default LandingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(104, 225, 253, 0.2)',
    justifyContent: 'center',
  },
  logo: {
    width: 200,
    height: 200,
  },
  logoContainer: {
    alignSelf: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    marginVertical: 13,
    marginHorizontal: 16,
    borderRadius: 3,
    width: 120,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 8,
    color: '#07689F',
    marginHorizontal: 16,
  },
  bodyText: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginBottom: 16,
    textAlign: 'center',
    color: '#07689F',
    marginHorizontal: 16,
    marginTop: 8,
  },
  slide: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide1Image: {
    width: 400,
    height: 320,
    marginVertical: 16,
  },
});

import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Menu, Modal} from 'react-native-paper';
import {Icon, Header, Button, Image} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import {getMember, inviteMember, deleteMember} from '../../redux/Action';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import AwesomeAlert from 'react-native-awesome-alerts';
import {ALERT} from '../../redux/Case Type/Project';

const MyCenterComponent = props => {
  return <Text style={styles.headerText}>{props.title}</Text>;
};

const MyLeftComponent = props => {
  return (
    <Icon
      type="feather"
      name="arrow-left"
      size={24}
      containerStyle={styles.icon}
      onPress={props.back}
    />
  );
};

const Member = () => {
  const navigation = useNavigation();
  const title = useSelector(state => state.project.title);
  const dispatch = useDispatch();
  const idproject = useSelector(state => state.project.idProject);
  const [index, setIndex] = useState(0);
  const Members = useSelector(state => state.project.projectMember);
  const [visible, setVisible] = useState(false);
  const [email, setEmail] = useState('');
  const message = useSelector(state => state.project.message);
  const alert = useSelector(state => state.project.alert);

  useEffect(() => {
    dispatch(getMember(idproject));
  }, [dispatch, idproject, Members]);

  const showMenu = id => {
    setIndex(id);
  };

  const hideMenu = () => {
    setIndex(0);
  };

  const onChangeEmail = val => {
    setEmail(val);
  };

  const handleInviteMember = () => {
    dispatch(inviteMember(idproject, email));
    setVisible(false);
  };

  const handleDelete = id => {
    dispatch(deleteMember(id));
    setIndex(0);
  };

  const renderItem = ({item}) => (
    <View style={styles.projectCardBar}>
      <View style={styles.projectBodyContainer}>
        <Image source={{uri: item.image_url}} style={styles.projectImage} />
        <Text style={styles.projectCardTitle}>{item.name}</Text>
        <Text style={styles.role}>{item.role}</Text>
      </View>
      <Menu
        visible={index === item.id ? true : false}
        onDismiss={hideMenu}
        anchor={
          <Icon
            type="feather"
            name="more-vertical"
            size={24}
            containerStyle={styles.projectCardMore}
            onPress={() => showMenu(item.id)}
          />
        }>
        {item.role !== 'owner' && (
          <Menu.Item
            title="Delete Member"
            onPress={() => handleDelete(item.id)}
          />
        )}
      </Menu>
    </View>
  );

  return (
    <>
      <Header
        leftComponent={<MyLeftComponent back={() => navigation.goBack()} />}
        // rightComponent={<MyRightComponent navigation={navigation} />}
        centerComponent={<MyCenterComponent title={`${title}'s Member`} />}
        backgroundColor="#FFF"
        containerStyle={styles.containerHeader}
      />
      <View style={styles.container}>
        <View style={styles.projectContainer}>
          <Text style={styles.projectTitle}>Team Member</Text>
        </View>
        <View style={styles.projectCardContainer}>
          <FlatList
            data={Members}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        </View>
        <View style={styles.projectButtonContainer}>
          <Button
            icon={<Icon type="feather" name="plus" size={35} color="#FFF" />}
            containerStyle={styles.buttonContainer}
            onPress={() => setVisible(!visible)}
          />
        </View>
      </View>
      <Modal onDismiss={() => setVisible(false)} visible={visible}>
        <View style={styles.modalContainer}>
          <Text style={[styles.headerText, {marginVertical: 8}]}>
            Invite Member
          </Text>
          <TextInput
            placeholder="Input email address"
            style={styles.inputBox}
            onChangeText={onChangeEmail}
          />
          <Button
            title="Invite"
            containerStyle={styles.button}
            buttonStyle={styles.buton}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            onPress={handleInviteMember}
          />
        </View>
      </Modal>
      <AwesomeAlert
        show={alert}
        showProgress={false}
        title="Oops"
        message={message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="Oke"
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          dispatch({type: ALERT});
        }}
      />
    </>
  );
};

export default Member;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  containerHeader: {
    paddingTop: 10,
    height: 60,
    justifyContent: 'center',
  },
  inputBox: {
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    backgroundColor: '#FFF',
    marginHorizontal: 8,
    marginVertical: 16,
    fontSize: 16,
    fontFamily: 'Montserrat',
    paddingHorizontal: 14,
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
    color: '#000',
  },
  projectTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'center',
  },
  projectContainer: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
  projectCardContainer: {
    marginHorizontal: 16,
    marginVertical: 10,
    paddingBottom: 75,
  },
  projectCardBar: {
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 3,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 4,
  },
  projectImage: {
    width: 62,
    height: 62,
  },
  projectCardTitle: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginHorizontal: 30,
  },
  projectCardMore: {
    marginHorizontal: 16,
  },
  buttonContainer: {
    borderRadius: 100,
    alignSelf: 'center',
  },
  projectButtonContainer: {
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'flex-end',
    bottom: 30,
    right: 30,
  },
  projectBodyContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  role: {
    color: '#C4C4C4',
    fontFamily: 'Montserrat',
  },
  modalContainer: {
    backgroundColor: '#FFF',
    marginHorizontal: 16,
  },
  buton: {
    alignSelf: 'center',
    marginVertical: 16,
  },
});

import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  TextInput,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import {Image, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-picker';
import {getProjectDetail, editProject} from '../../redux/Action/ProjectAction';

const EditProject = () => {
  const navigation = useNavigation();
  const [title, setTitle] = useState('');
  const [descrip, setDescrip] = useState('');
  const dispatch = useDispatch();
  const [filePath, setFilePath] = useState('');
  const loading = useSelector(state => state.project.isLoading);
  const idproject = useSelector(state => state.project.idProject);
  const project = useSelector(state => state.project);
  const projectTitle = project.title;
  const projectDescrip = project.description;
  const projectImage = project.imageProject;

  const onChangeTitle = val => {
    setTitle(val);
  };

  const onChangeDescrip = val => {
    setDescrip(val);
  };

  useEffect(() => {
    dispatch(getProjectDetail(idproject));
  }, [dispatch, idproject]);

  const ChooseFile = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        setFilePath(response);
      }
    });
  };

  const handleEditProject = () => {
    let dataForm = new FormData();
    if (title !== '') {
      dataForm.append('title', title);
    }
    if (descrip !== '') {
      dataForm.append('description', descrip);
    }
    if (filePath !== '') {
      dataForm.append('image', {
        name: filePath.fileName,
        type: filePath.type,
        uri: filePath.uri,
      });
    }
    dispatch(editProject(idproject, dataForm, navigation));
  };

  let fileImage;
  if (filePath !== '') {
    fileImage = {uri: filePath.uri};
  } else {
    fileImage = {uri: projectImage};
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Title</Text>
          <TextInput
            placeholder="Title"
            style={styles.titleInput}
            onChangeText={onChangeTitle}
            defaultValue={projectTitle}
          />
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.title}>Description</Text>
          <TextInput
            placeholder="Description"
            multiline={true}
            style={styles.descriptionInput}
            onChangeText={onChangeDescrip}
            defaultValue={projectDescrip}
          />
        </View>
        <View style={styles.imageContainer}>
          <Text style={styles.title}>Project Image</Text>
          <TouchableWithoutFeedback onPress={ChooseFile}>
            <Image source={fileImage} containerStyle={styles.image} />
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title="Save Changes"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={handleEditProject}
          />
          <Button
            title="Cancel"
            containerStyle={styles.button}
            titleStyle={styles.cancelTitle}
            buttonStyle={styles.cancelButton}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ScrollView>
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default EditProject;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  titleContainer: {
    marginHorizontal: 16,
    marginTop: 28,
  },
  title: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  titleInput: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginTop: 8,
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionInput: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginTop: 8,
    height: 100,
    textAlignVertical: 'top',
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  image: {
    width: 100,
    height: 100,
    marginTop: 8,
  },
  imageContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  button: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
  buttonContainer: {
    marginTop: 50,
  },
});

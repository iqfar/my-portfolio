import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Icon, Image, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useDispatch, useSelector} from 'react-redux';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import {API_USER} from '../../API';
import {useNavigation} from '@react-navigation/native';
import {
  GET_NAME,
  GET_BIO,
  GET_PROFILE_IMAGE,
} from '../../redux/Case Type/Showuser';

const EditProfile = () => {
  const profile = useSelector(state => state.showUser);
  const username = profile.name;
  const biodata = profile.bio;
  const email = profile.email;
  const image = profile.imageProfile;
  const [name, setName] = useState('');
  const [bio, setBio] = useState('');
  const [avatar, setAvatar] = useState('');
  const [filePath, setFilePath] = useState('');
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const onChangeName = val => {
    setName(val);
  };

  const onChangeBio = val => {
    setBio(val);
  };

  const ChooseFile = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        setAvatar(response.uri);
        setFilePath(response);
      }
    });
  };

  const handleUpdateProfile = async () => {
    let dataForm = new FormData();
    if (name !== '') {
      dataForm.append('name', name);
      dispatch({type: GET_NAME, name: name});
    }
    if (bio !== '') {
      dataForm.append('bio', bio);
      dispatch({type: GET_BIO, bio: bio});
    }
    if (filePath !== '') {
      dataForm.append('image', {
        name: filePath.fileName,
        type: filePath.type,
        uri: filePath.uri,
      });
      dispatch({type: GET_PROFILE_IMAGE, imageProfile: avatar});
    }
    setLoading(true);
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.put(`${API_USER}/update`, dataForm, {
        headers: {
          auth_token: token,
        },
      });
      if (res !== null) {
        const status = res.data.status;
        if (status == 'success') {
          setLoading(false);
          navigation.navigate('Profile');
        } else {
          setLoading(false);
        }
      }
    } catch (error) {
      console.log(error, 'error');
      setLoading(false);
    }
  };
  let avatarImage;
  if (avatar !== '') {
    avatarImage = avatar;
  } else {
    avatarImage = image;
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.imageContainer}>
          <Image source={{uri: avatarImage}} style={styles.imageProfile} />
          <Icon
            type="feather"
            name="edit"
            size={25}
            containerStyle={styles.icon}
            onPress={ChooseFile}
          />
        </View>
        <View style={styles.usernameContainer}>
          <TextInput
            defaultValue={username}
            style={styles.username}
            onChangeText={val => onChangeName(val)}
          />
        </View>
        <View style={styles.profileContainer}>
          <View style={styles.emailContainer}>
            <View
              style={{justifyContent: 'space-between', flexDirection: 'row'}}>
              <Text style={styles.profileTitle}>Email</Text>
              <Button
                title="Change Password"
                containerStyle={{alignSelf: 'flex-end'}}
                ViewComponent={LinearGradient}
                linearGradientProps={LinearConfig}
                onPress={() => navigation.navigate('Update Password')}
              />
            </View>
            <Text style={styles.email}>{email}</Text>
          </View>
          <View style={styles.biodataContainer}>
            <Text style={styles.profileTitle}>Bio</Text>
            <TextInput
              defaultValue={biodata}
              style={styles.biodata}
              multiline={true}
              onChangeText={val => onChangeBio(val)}
            />
          </View>
        </View>
        <View style={styles.editProfileContainer}>
          <Button
            title="Save"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={handleUpdateProfile}
          />
          <Button
            title="Cancel"
            containerStyle={styles.button}
            titleStyle={styles.cancelTitle}
            buttonStyle={styles.cancelButton}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ScrollView>
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  containerHeader: {
    paddingTop: 10,
    height: 60,
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
    color: '#000',
    width: 100,
  },
  imageProfile: {
    width: 105,
    height: 105,
    borderWidth: 2,
    borderColor: '#1589CB',
    borderRadius: 100,
  },
  imageContainer: {
    alignSelf: 'center',
    marginVertical: 25,
    flexDirection: 'row',
  },
  username: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 24,
    backgroundColor: '#fff',
  },
  usernameContainer: {
    marginHorizontal: 16,
    marginBottom: 24,
  },
  profileTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  email: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginVertical: 8,
  },
  emailContainer: {
    marginHorizontal: 16,
    marginVertical: 8,
  },
  biodataContainer: {
    marginHorizontal: 16,
    marginVertical: 8,
  },
  biodata: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginVertical: 8,
    backgroundColor: '#fff',
    textAlign: 'left',
  },
  button: {
    marginTop: 20,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  icon: {
    alignSelf: 'flex-end',
    left: -30,
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
});

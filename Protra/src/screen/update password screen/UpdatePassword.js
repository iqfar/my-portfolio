import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import AwesomeAlert from 'react-native-awesome-alerts';
import {TextInput, ScrollView} from 'react-native-gesture-handler';
import {Button, Icon} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import {API_USER} from '../../API';

const UpdatePassword = () => {
  const navigation = useNavigation();
  const [password, setPassword] = useState('');
  const [cPassword, setCPassword] = useState('');
  const [oldPassword, setOldPassword] = useState('');
  const [alert, setAlert] = useState(false);
  const [hidden, setHidden] = useState(true);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const [success, setSuccess] = useState(false);

  const onChangePassword = val => {
    setPassword(val);
  };

  const onChangeCPassword = val => {
    setCPassword(val);
  };

  const onChangeOldPassword = val => {
    setOldPassword(val);
  };

  const handleUpdatePassword = async () => {
    setLoading(true);
    if (password === cPassword) {
      try {
        const token = await AsyncStorage.getItem('userToken');
        const res = await Axios.put(
          `${API_USER}/changePassword`,
          {
            currentPassword: oldPassword,
            newPassword: password,
          },
          {
            headers: {
              auth_token: token,
            },
          },
        );

        if (res !== null) {
          const status = res.data.status;
          if (status == 'success') {
            setLoading(false);
            setSuccess(true);
          } else {
            setLoading(false);
          }
        }
      } catch (error) {
        console.log(error, 'error');
        setLoading(false);
        setMessage(error.response.data.message);
        setAlert(true);
      }
    } else {
      setMessage('Password not match');
      setAlert(true);
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.inputContainer}>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Old Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangeOldPassword}
            />
            <Icon
              type="feather"
              name={hidden ? 'eye-off' : 'eye'}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
          </View>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>New Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangePassword}
            />
            <Icon
              type="feather"
              name={hidden ? 'eye-off' : 'eye'}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
            <Text style={{fontFamily: 'Montserrat', fontSize: 10}}>
              Password minimum eight characters, contain uppercase, lowercase
              and number
            </Text>
          </View>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Confirm New Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangeCPassword}
            />
            <Icon
              type="feather"
              name={hidden ? 'eye-off' : 'eye'}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title="Change Password"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={handleUpdatePassword}
          />
          <Button
            title="Cancel"
            containerStyle={styles.button}
            titleStyle={styles.cancelTitle}
            buttonStyle={styles.cancelButton}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ScrollView>
      <AwesomeAlert
        show={alert}
        showProgress={false}
        title="Oops"
        message={message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="Oke"
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          setAlert(false);
        }}
      />
      <AwesomeAlert
        show={success}
        showProgress={false}
        title="Great!!"
        message="Password Updated"
        showConfirmButton={true}
        confirmText="Nice"
        confirmButtonColor="#25ed21"
        onConfirmPressed={() => {
          navigation.navigate('Profile');
          setSuccess(false);
        }}
      />
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default UpdatePassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'flex-start',
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 30,
  },
  logo: {
    width: 15,
    height: 15,
    marginHorizontal: 5,
  },
  logoText: {
    fontFamily: 'Metropolis',
    fontWeight: 'bold',
    fontSize: 18,
    letterSpacing: -0.7,
  },
  inputContainer: {
    marginHorizontal: 16,
  },
  inputBar: {
    justifyContent: 'flex-start',
    marginVertical: 10,
  },
  inputText: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  inputBox: {
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    backgroundColor: '#FFF',
    marginTop: 8,
    paddingHorizontal: 14,
  },
  button: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  signup: {
    alignSelf: 'flex-end',
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 16,
  },
  icon: {
    alignSelf: 'flex-end',
    position: 'absolute',
    top: 45,
    right: 15,
  },
  warning: {
    color: '#ff0d00',
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
});

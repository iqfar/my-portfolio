import {
  PROCCESS,
  SUCCESS,
  FAILED,
  RESTORE_TOKEN,
  GET_DATA,
  DETAIL,
  COMMENT,
  GENRE,
  GENRE_NAME,
  COMMENT_EDIT,
  SEARCH,
  ISLOGIN,
  ISLOGOUT,
  ALERT,
} from './case';

const initialState = {
  user: {},
  data: [],
  token: null,
  isLoading: false,
  message: null,
  id: null,
  isComment: false,
  isLogin: false,
};

export const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case PROCCESS:
      return {
        ...state,
        isLoading: true,
      };
    case SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case FAILED:
      return {
        ...state,
        isLoading: false,
        message: alert('Invalid Input'),
      };
    case RESTORE_TOKEN:
      return {
        ...state,
        token: action.payload,
      };
    case GET_DATA:
      return {
        ...state,
        data: action.payload,
      };
    case DETAIL:
      return {
        ...state,
        id: action.payload,
      };
    case SEARCH:
      return {
        ...state,
        title: action.payload,
      };
    case ISLOGIN:
      return {
        ...state,
        isLogin: true,
      };
    case ISLOGOUT:
      return {
        ...state,
        isLogin: false,
      };
    case ALERT:
      return {
        ...state,
        alert: !state.alert,
      };
    default:
      return state;
  }
};

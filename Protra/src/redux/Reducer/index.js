import {combineReducers} from 'redux';
import {AuthReducer} from './AuthReducer';
import {ShowUserReducer} from './ShowUserReducer';
import {ProjectReducer} from './ProjectReducer';
import {TaskReducer} from './TaskReducer';

const reducer = combineReducers({
  auth: AuthReducer,
  showUser: ShowUserReducer,
  project: ProjectReducer,
  task: TaskReducer,
});

export default reducer;

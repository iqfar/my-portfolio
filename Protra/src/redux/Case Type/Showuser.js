export const GET_EMAIL = 'GET_EMAIL';
export const GET_NAME = 'GET_NAME';
export const GET_BIO = 'GET_BIO';
export const GET_PROFILE_ID = 'GET_PROFILE_ID';
export const GET_PROFILE_IMAGE = 'GET_PROFILE_IMAGE';

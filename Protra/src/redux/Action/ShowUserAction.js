import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import {API_USER} from '../../API';
import {
  GET_PROFILE_ID,
  GET_PROFILE_IMAGE,
  GET_EMAIL,
  GET_NAME,
  GET_BIO,
} from '../Case Type/Showuser';

export const getUser = () => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.get(`${API_USER}/show`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_PROFILE_IMAGE,
          imageProfile: data.image_url,
        });
        dispatch({type: GET_EMAIL, email: data.email});
        dispatch({type: GET_NAME, name: data.name});
        dispatch({type: GET_BIO, bio: data.bio});
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const getNews = () => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.get(
        'https://ga-protra.herokuapp.com/api/v1/news_feed',
        {
          headers: {
            auth_token: token,
          },
        },
      );

      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: 'GET_HISTORY',
          history: data,
        });
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import {API_PROJECT} from '../../API';
import {
  GET_PROJECT_ID,
  GET_PROJECT_ALL,
  GET_PROJECT_TITLE,
  GET_PROJECT_IMAGE,
  GET_PROJECT_CARD,
  GET_PROJECT_DESCRIPTION,
  STATUS,
  LOADING,
  GET_PROJECT_MEMBER,
  MESSAGE,
} from '../Case Type/Project';

export const getAllProject = () => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.get(`${API_PROJECT}/showAll`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_PROJECT_ALL,
          project: data.project,
          task: data.total_task,
        });
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const getProjectDetail = id => {
  return async dispatch => {
    dispatch({type: LOADING, isLoading: true});
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.get(`${API_PROJECT}/showOne/${id}`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_PROJECT_TITLE,
          title: data.title,
        });
        dispatch({
          type: GET_PROJECT_DESCRIPTION,
          description: data.description,
        });
        dispatch({
          type: GET_PROJECT_IMAGE,
          imageProject: data.image,
        });
        dispatch({
          type: GET_PROJECT_CARD,
          cardProject: data.card,
        });
        dispatch({type: LOADING, isLoading: false});
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: LOADING, isLoading: false});
    }
  };
};

export const createProject = (dataForm, navigation) => {
  return async dispatch => {
    dispatch({type: LOADING, isLoading: true});
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.post(`${API_PROJECT}/create`, dataForm, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        const status = res.data.status;
        dispatch({type: STATUS, status: status});
        dispatch({type: LOADING, isLoading: false});
        if (status == 'success') {
          navigation.navigate('Project');
        }
      }
    } catch (error) {
      console.log('error', error);
      dispatch({type: LOADING, isLoading: false});
    }
  };
};

export const deleteProject = id => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.delete(`${API_PROJECT}/delete/${id}`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        console.log('Project deleted');
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const editProject = (id, dataForm, navigation) => {
  return async dispatch => {
    try {
      dispatch({type: LOADING, isLoading: true});
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.put(`${API_PROJECT}/update/${id}`, dataForm, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        const status = res.data.status;
        dispatch({type: STATUS, status: status});
        dispatch({type: LOADING, isLoading: false});
        if (status == 'success') {
          navigation.navigate('Project');
        }
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: LOADING, isLoading: false});
    }
  };
};

export const inviteMember = (id, member) => {
  return async dispatch => {
    try {
      dispatch({type: LOADING, isLoading: true});
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.post(
        `${API_PROJECT}/member/invite/${id}`,
        {
          email: member,
        },
        {
          headers: {
            auth_token: token,
          },
        },
      );

      if (res !== null) {
        console.log(res.data);
        dispatch({type: LOADING, isLoading: false});
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: LOADING, isLoading: false});
      dispatch({type: MESSAGE, message: error.response.data.message});
    }
  };
};

export const deleteMember = id => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.delete(`${API_PROJECT}/member/kick/${id}`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        console.log(res.data);
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const getMember = id => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.get(`${API_PROJECT}/member/showAll/${id}`, {
        headers: {
          auth_token: token,
        },
      });
      if (res !== null) {
        const data = res.data.data;
        dispatch({type: GET_PROJECT_MEMBER, projectMember: data});
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import {API_TASKS} from '../../API';
import {
  GET_TASK_ID,
  GET_TASK_TITLE,
  GET_TASK_DESCRIPTION,
  GET_TASK_CARD_ID,
  GET_TASK_IMAGE,
  TASK_STATUS,
  TASK_LOADING,
  TASK_ASSIGNMENT,
  TASK_MESSAGE,
} from '../Case Type/Task';
import {useSelector} from 'react-redux';

export const getTask = id => {
  return async dispatch => {
    dispatch({
      type: TASK_LOADING,
      isLoading: true,
    });
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.get(`${API_TASKS}/showOne/${id}`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_TASK_ID,
          idTask: data.id,
        });
        dispatch({
          type: GET_TASK_TITLE,
          title: data.title,
        });
        dispatch({
          type: GET_TASK_DESCRIPTION,
          description: data.description,
        });
        dispatch({
          type: GET_TASK_CARD_ID,
          cardId: data.card_id,
        });
        dispatch({
          type: GET_TASK_IMAGE,
          image: data.Images,
        });
        dispatch({
          type: TASK_LOADING,
          isLoading: false,
        });
        dispatch({
          type: TASK_ASSIGNMENT,
          assignment: data.assignment,
        });
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({
        type: TASK_LOADING,
        isLoading: false,
      });
    }
  };
};

export const createTask = (idProject, dataForm, navigation) => {
  return async dispatch => {
    dispatch({
      type: TASK_LOADING,
      isLoading: true,
    });
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.post(
        `${API_TASKS}/create/${idProject}`,
        dataForm,
        {
          headers: {
            auth_token: token,
          },
        },
      );

      if (res !== null) {
        const status = res.data.status;
        dispatch({
          type: TASK_STATUS,
          status: status,
        });
        dispatch({
          type: TASK_LOADING,
          isLoading: false,
        });
        if (status == 'success') {
          navigation.navigate('Project Detail');
          navigation.reset({
            index: 1,
            routes: [{name: 'Project Detail'}],
          });
        }
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({
        type: TASK_LOADING,
        isLoading: false,
      });
    }
  };
};

export const deleteTask = id => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.delete(`${API_TASKS}/delete/${id}`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        console.log('Task Deleted');
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const editTask = (id, dataForm, navigation) => {
  return async dispatch => {
    dispatch({type: TASK_LOADING, isLoading: true});
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.put(`${API_TASKS}/update/${id}`, dataForm, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        const status = res.data.status;
        if (status == 'success') {
          navigation.navigate('Project Detail');
          navigation.reset({
            index: 0,
            routes: [{name: 'Project Detail'}],
          });
        }
        dispatch({
          type: TASK_LOADING,
          isLoading: false,
        });
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({
        type: TASK_LOADING,
        isLoading: false,
      });
    }
  };
};

export const editSelectProgressTask = (id, navigation, card) => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const projectId = useSelector(state => state.projet.idProject);
      const res = await Axios.put(
        `${API_TASKS}/update/${id}`,
        {
          card_id: `${card}${projectId}`,
        },
        {
          headers: {
            auth_token: token,
          },
        },
      );
      if (res !== null) {
        console.log('Task Success Update');
        navigation.reset({
          index: 0,
          routes: [{name: 'Project Detail'}],
        });
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const editSwipeRightProgressTask = (id, navigation, projectId) => {
  return async dispatch => {
    try {
      dispatch({type: 'MOVING', moving: 'Moving...'});
      const token = await AsyncStorage.getItem('userToken');
      const task = await Axios.get(`${API_TASKS}/showOne/${id}`, {
        headers: {
          auth_token: token,
        },
      });

      let card_right;
      if (task.data.data.card_id == `Backlog${projectId}`) {
        card_right = `Todo${projectId}`;
      } else if (task.data.data.card_id == `Todo${projectId}`) {
        card_right = `Progress${projectId}`;
      } else if (task.data.data.card_id == `Progress${projectId}`) {
        card_right = `Done${projectId}`;
      } else {
        card_right = null;
      }

      if (task !== null) {
        try {
          const res = await Axios.put(
            `${API_TASKS}/update/${id}`,
            {
              card_id: card_right,
            },
            {
              headers: {
                auth_token: token,
              },
            },
          );
          if (res !== null) {
            dispatch({type: 'MOVING', moving: null});
            console.log('Task Success Update');
            navigation.reset({
              index: 0,
              routes: [{name: 'Project Detail'}],
            });
          }
        } catch (error) {
          console.log('error', error);
          dispatch({type: 'MOVING', moving: null});
        }
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: 'MOVING', moving: null});
    }
  };
};

export const editSwipeLeftProgressTask = (id, navigation, projectId) => {
  return async dispatch => {
    try {
      dispatch({type: 'MOVING', moving: 'Moving...'});
      const token = await AsyncStorage.getItem('userToken');
      const task = await Axios.get(`${API_TASKS}/showOne/${id}`, {
        headers: {
          auth_token: token,
        },
      });
      let card_left;
      if (task.data.data.card_id == `Done${projectId}`) {
        card_left = `Progress${projectId}`;
      } else if (task.data.data.card_id == `Progress${projectId}`) {
        card_left = `Todo${projectId}`;
      } else if (task.data.data.card_id == `Todo${projectId}`) {
        card_left = `Backlog${projectId}`;
      } else {
        card_left = null;
      }

      if (task !== null) {
        try {
          const res = await Axios.put(
            `${API_TASKS}/update/${id}`,
            {
              card_id: card_left,
            },
            {
              headers: {
                auth_token: token,
              },
            },
          );
          if (res !== null) {
            dispatch({type: 'MOVING', moving: null});
            console.log('Task Success Update');
            navigation.reset({
              index: 0,
              routes: [{name: 'Project Detail'}],
            });
          }
        } catch (error) {
          console.log('error', error);
          dispatch({type: 'MOVING', moving: null});
        }
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: 'MOVING', moving: null});
    }
  };
};

export const assignmentTask = (id, email, assignment) => {
  return async dispatch => {
    try {
      dispatch({type: TASK_LOADING, isLoading: true});
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.post(
        `${API_TASKS}/assignment/${id}`,
        {
          email: email,
        },
        {
          headers: {
            auth_token: token,
          },
        },
      );

      if (res !== null) {
        dispatch({type: TASK_LOADING, isLoading: false});
        dispatch(getTask(id));
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: TASK_LOADING, isLoading: false});
      dispatch({type: TASK_MESSAGE, message: error.response.data.message});
    }
  };
};

export const assignmentDelete = (id, assignment) => {
  return async dispatch => {
    dispatch({type: TASK_LOADING, isLoading: true});
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.delete(`${API_TASKS}/revokeAssignment/${id}`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        dispatch({type: TASK_LOADING, isLoading: false});
        const arr = assignment.filter(item => {
          return item.id !== id;
        });
        dispatch({type: TASK_ASSIGNMENT, assignment: arr});
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: TASK_LOADING, isLoading: false});
    }
  };
};

export const deleteImage = id => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.delete(`${API_TASKS}/delete_image/${id}`, {
        headers: {
          auth_token: token,
        },
      });
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

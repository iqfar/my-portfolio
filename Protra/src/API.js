export const API_USER = 'https://ga-protra.herokuapp.com/api/v1/user';
export const API_REGISTER =
  'https://ga-protra.herokuapp.com/api/v1/user/register';
export const API_LOGIN = 'https://ga-protra.herokuapp.com/api/v1/user/login';
export const API_PROJECT = 'https://ga-protra.herokuapp.com/api/v1/project';
export const API_TASKS = 'https://ga-protra.herokuapp.com/api/v1/task';

import React, { useState } from 'react';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { StyleSheet} from 'react-native';
import MyDrawer from './Drawer';
import Spinner from 'react-native-loading-spinner-overlay';
import { connect, useSelector } from 'react-redux';

// const Drawer = createDrawerNavigator();
//  function MyDrawer() {
//     const [userToken, setUserToken] = useState('')
//     const [auth, setAuth] = useState('')
//     const [isLogin, setIsLogin] = useState(false);
    
//     try{
//         AsyncStorage.getItem('userToken').then((token) => {
//             if(token){
//                 setUserToken(token)
//                 setAuth(token)
//                 setIsLogin(true)
//             }else{
//                 console.log('no token')
//             }
//         })
//     }catch(err){
//         console.log('error')
//     }


//     const handleLogout = async () =>{
//         try {
//            AsyncStorage.removeItem('userToken')
//            .then(setIsLogin(false))
//         } catch (error) {
//             console.log('error')
//         }
        
//     }

    

//     return(
//             <Drawer.Navigator drawerContent={(props) => <CustomDrawer {...props} logout={handleLogout} />}>
//                 {isLogin == false ? (
//                 <Drawer.Screen name='Login' component={Routes} options={{ gestureEnabled: false }} /> ) : (
//                     <>
//                 <Drawer.Screen name='Home' component={HomeScreen} options={{ drawerLabel:'Dashboard' , drawerIcon: ({}) => <Icon type='material-community' name='home' size={40} color='#448ce3'  /> }} />
//                 <Drawer.Screen name='Profil' component={Profile} logout={handleLogout} options={{ drawerLabel: 'Profile', drawerIcon: ({}) => <Icon type='material-community' name='account' size={40} color='#448ce3' /> }} />
//                     </>
//                 )}
//             </Drawer.Navigator>
//     )
// }

const AppNavigator = () => {
    const isLoading = useSelector(state => state.auth.isLoading)
    return(
        <NavigationContainer>
            <Spinner 
            visible={isLoading}
            />
            <MyDrawer />
        </NavigationContainer>
    )
}


export default AppNavigator;
const styles = StyleSheet.create({
    Drawer: {
        
    }
})


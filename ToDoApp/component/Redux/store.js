import reducer from './Reducer/AppReducer';
import { createStore, combineReducers } from 'redux';

export const store = createStore(reducer)
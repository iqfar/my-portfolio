import { LOADING, LOGIN } from "./case";

const initialState = {
    user: {},
    data: [],
    name: '',
    description: '',
    isLogin: false,
    token: '',
    isLoading: false,
}

export const AuthReducer = (state = initialState, action) => {
    switch(action.type){
        case 'ERROR': 
            return {...state, description: action.payload};
        case 'SAVE':
            return {...state, data: action.payload};
        case 'CHANGE_NAME': 
            return {...state, name: action.payload}
        case 'LOGOUT':
            return {...state, isLogin: action.payload }
        case LOGIN:
            return {...state, token: action.payload}
        case LOADING: 
            return {...state, isLoading: action.payload}
        default: 
            return state;
    }
} 
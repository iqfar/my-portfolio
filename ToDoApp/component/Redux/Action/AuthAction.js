export const save_data = (data) => {
    return{
        type: 'SAVE',
        payload: data,
    }
}

export const change_name = (params) =>{
    return{
        type: 'CHANGE_NAME',
        payload: params,
    }
}

export const handleLogout = (state) => {
    return{
        type: 'LOGOUT',
        payload: state
    }
}

export const handleLogin = async (token) => {
    return{
        type: 'LOGIN',
        payload: token
    }
}

export const handleLoading = (state) => {
    return{
        type: "LOADING",
        payload: state
    }
}
//API Resource 

import AsyncStorage from '@react-native-community/async-storage';

export const API_LOGIN = 'https://miniproject-team-a.herokuapp.com/api/v1/login'
export const API_REGISTER = 'https://miniproject-team-a.herokuapp.com/api/v1/register'
export const API_TASKS = 'https://miniproject-team-a.herokuapp.com/api/v1/tasks'
export const API_USER = 'https://miniproject-team-a.herokuapp.com/api/v1/user'

export const AUTH_TOKEN = AsyncStorage.getItem('userToken')
import React, { useState } from 'react'
import { DrawerContent, DrawerItem } from '@react-navigation/drawer'
import DefaultImage from './asset/defaultDrawer.png';
import { Image, Icon } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import Routes from './login screen/Router';
import { createDrawerNavigator, DrawerItemList, DrawerContentScrollView } from '@react-navigation/drawer';
import HomeScreen from './main screen/index';
import Profile from './main screen/Profile';
import Axios from 'axios';
import { API_USER } from './API';
import { Text } from 'react-native'
import Important from './main screen/Important';
import Complete from './main screen/Complete';
import { useDispatch } from 'react-redux';
import { handleLoading, handleLogin } from './Redux/Action';



const CustomDrawer = (props) => {

    const {navigation} = {...props}
    
    
    

    return (
        <DrawerContentScrollView {...props}>
            <DrawerItem 
                label={() => <Text>{props.name}</Text>}
                icon={() => <Image source={props.image} defaultSource={DefaultImage} style={{
                    width: 120,
                    height: 120,
                    maxHeight: 120,
                    maxWidth: 120,
                    borderRadius: 100,
                    alignSelf: 'center'
                }} />}
                labelStyle={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    lineHeight: 21,
                    color: '#3181EC',
                    // display: 'none'
                }}
                style={{
                    alignSelf: 'center',
                    margin: 10,
                    marginVertical: 20,
                    marginBottom: 50,
                }}
            />
            
            <DrawerItemList {...props}  />
            <DrawerItem 
            label='Sign Out'
            icon={() => <Icon name='logout' type='material-community' size={40} color='#448ce3' />}
            style={{ marginTop: 100 }}
            onPress={props.logout}
            />
        </DrawerContentScrollView>
    )
}

const Drawer = createDrawerNavigator();
 function MyDrawer() {
    const [userToken, setUserToken] = useState('')
    const [auth, setAuth] = useState('')
    const [isLogin, setIsLogin] = useState(false);
    const [name, setName] = useState('');
    const [image, setImage] = useState('');
    // const username = AsyncStorage.getItem('userName');
    // const userImage = AsyncStorage.getItem('userImage')
    // const dispatch = useDispatch();
    // dispatch(handleLoading(true))
    try{
        AsyncStorage.getItem('userToken').then((token) => {
            // dispatch(handleLoading(false))
            if(token){
                setUserToken(token)
                setAuth(token)
                setIsLogin(true)
                // dispatch(handleLogin(userToken))
                // alert(token)

            }else{
                console.log('no token')
            }
        })
        // const res = Axios.get(API_USER, {
        //     headers: {
        //         'Authorization': userToken
        //     }
        // })

        // if(res !== null){
        //     const data = res.data
        //     const username = data.name
        //     const userImage = data.image_url
        //     setName(username);
        //     setImage(userImage)
        //     console.log(name)
        //     console.log(image)
        // }else{
        //     console.log('error');
        // }
    }catch(err){
        console.log('error')
        // dispatch(handleLoading(false))
    }
    console.log(name)
    console.log(image)
    console.log(userToken)
    try{
        Axios.get(API_USER, {
            headers: {
                'Authorization': userToken
            }
        }).then(res => {
            const data = res.data
            setName(data.data.name)
            setImage(data.data.image_url)
        })
    }catch(error){
        console.log('error')
    }

    // dispatch(handleLoading(false))

    const handleLogout = async () =>{
        // dispatch(handleLoading(true))
        try {
           AsyncStorage.removeItem('userToken')
        //    .then(setIsLogin(false), dispatch(handleLoading(false)) )
        } catch (error) {
            console.log('error')
            // dispatch(handleLoading(false))
        }
        
    }

    

    return(
            <Drawer.Navigator drawerContent={(props) => <CustomDrawer {...props} name={name} image={{ uri: `${image}` }}  logout={handleLogout} />}>
                {isLogin == false ? (
                <Drawer.Screen name='Login' component={Routes} options={{ gestureEnabled: false }} /> ) : (
                    <>
                <Drawer.Screen name='Home' component={HomeScreen} options={{ drawerLabel:'Dashboard' , drawerIcon: ({}) => <Icon type='material-community' name='home' size={40} color='#448ce3'  /> }} />
                <Drawer.Screen name='Profil' component={Profile} logout={handleLogout} options={{ drawerLabel: 'Profile', drawerIcon: ({}) => <Icon type='material-community' name='account' size={40} color='#448ce3' /> }} />
              <Drawer.Screen name='Important Tasks' component={Important} options={{ drawerLabel: 'Important Tasks', drawerIcon: ({}) => <Icon type='material-community' name='star' size={40} color='#448ce3' /> }} /> 
                <Drawer.Screen name='Completed Tasks' component={Complete} options={{ drawerLabel: 'Completed Tasks', drawerIcon: ({}) => <Icon type='material-community' name='check-circle-outline' size={40} color='#448ce3' /> }} /> 
                    </>
                )}
            </Drawer.Navigator>
    )
}

export default MyDrawer;

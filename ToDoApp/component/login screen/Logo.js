import React, { Component } from 'react';
import logo from '../asset/login.png';
import { View, StyleSheet, Image, Text } from 'react-native';

export default class Logo extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.logo}>
                <Image style={styles.image} source={logo} />
                <Text style={styles.logoText}>TODO</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    logo: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
    },

    image:{
        position: 'absolute',
        width: 137,
        height: 121,
        top: 56,
        alignItems: 'center'
    },

    logoText: {
        position: 'absolute',
        width: 195,
        height: 30,
        top: 105,

        fontFamily: 'Roboto',
        fontWeight: "bold",
        fontSize: 18,
        lineHeight: 21,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',

        color: '#F2F2F2',
    }
})
import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Logo from './Logo';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import { API_REGISTER } from '../API';

class SignUp extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: "",
            c_password: '',
            username: '' ,
            firstName: "",
            lastName: ""
        }
    }

    onChangeText = (key, val) => {
        this.setState({ [key]: val })
    }

    handleRegister = () => {
        const {username, email, password} = this.state
        const { navigation } = this.props
        this.setState({ username: this.state.firstName + '' + this.state.lastName })
        try {
        axios.post(API_REGISTER, {
            email: email,
            password: password,
            name: username
        })
        .then(res => {
            const status = res.status
            if(status == 201){
                navigation.navigate('Splash')
            }else{
                alert('Register Gagal')
            }
            console.log(res.data)})
        } catch(err) {
            console.log('error')
        }
    }


    render(){

        const {navigation} = this.props;

        return(
            <LinearGradient colors={gradient} style={styles.gradient}>
            <Logo />
            <View style={styles.container}>
            <TextInput style={styles.inputBox} 
                placeholder='First Name'
                underlineColorAndroid='#ffffff'
                placeholderTextColor = "#ffffff"
                selectionColor="#fff"
                onChangeText={val => this.onChangeText('firstName', val)}
            />
            <TextInput style={styles.inputBox} 
                placeholder='Last Name'
                underlineColorAndroid='#ffffff'
                placeholderTextColor = "#ffffff"
                selectionColor="#fff"
                // value={this.state.lastName}
                onChangeText={val => this.onChangeText('lastName', val)}


            />
            <TextInput style={styles.inputBox} 
                    placeholder='Email'
                    keyboardType='email-address'
                    underlineColorAndroid='#ffffff'
                    placeholderTextColor = "#ffffff"
                    selectionColor="#fff"
                    onChangeText={val => this.onChangeText('email', val)}
                    // value={this.state.email}

            />
            <TextInput styles={styles.inputBox} 
                    placeholder='Password'
                    secureTextEntry={true}
                    underlineColorAndroid='#ffffff'
                    placeholderTextColor = "#ffffff"
                    selectionColor="#fff"
                    onChangeText={val => this.onChangeText('password', val)}
                    // value={this.state.password}
            />
            <TouchableOpacity style={styles.button}  onPress={this.handleRegister} >
                <Text style={styles.signupButton}>Register</Text>
            </TouchableOpacity>
            <View style={styles.signinTextCont}>
                <Text style={styles.signinText}>Already Have Account?</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}><Text style={styles.signinButton}> SignIn</Text></TouchableOpacity>
            </View>
            </View>
            </LinearGradient>
        )
    }
}

export default function(props){
    const navigation  = useNavigation();
    return <SignUp {...props} navigation={navigation} />
}

const gradient = ['#2F80ED', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    inputBox: {
        fontFamily: 'Roboto',
        fontSize: 18,
        lineHeight: 21,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'left',
        color: '#F2F2F2',
        marginVertical: 10,
    },
    gradient: {
        flex: 1,
    },
    container: {
        flex: 2,
    },
    button: {
        width: 150,
        height: 45,
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 18,
        lineHeight: 21,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        backgroundColor: '#0078D7',
        color: '#F2F2F2',
        alignSelf: 'center',
        //borderRadius: 100,
    },
    signupButton: {
        color: '#F2F2F2'
    },
    signinText: {
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontSize: 15,
        lineHeight: 18,
        display: 'flex',
        alignItems: 'center',
        textAlign: "right",
        color: '#F2F2F2',
    },
    signinButton: {
        fontWeight: 'bold',
        color: '#f2f2f2'
    },
    signinTextCont: {
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'row'
    }
})
import React, { Component } from 'react';
import { View, Image, StyleSheet, Button, Text, ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Logo from './Logo';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import ThirdLogin from './ThirdLogin';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import { API_LOGIN } from '../API';
import { connect } from 'react-redux';
import { handleLogin, handleLoading } from '../Redux/Action/index'

class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
        }
    }
    onChangeText = (key, val) => {
        this.setState({ [key]: val })
    }

    // handleLogin = async () => {
    //     const { email, password } = this.state
    //     const { navigation } = this.props
    //     try{
    //         await axios.post(API_LOGIN, {
    //             email: email,
    //             password: password
    //         })
    //         .then(res => {
    //             const status = res.status
    //             const data = res.data
    //             this.setState({ token: data.data.access_token})
    //             AsyncStorage.setItem('userToken', this.state.token)
    //             if(status == 200){
    //                 navigation.navigate('Home')
    //                 // console.log(data)
    //                 const AuthToken = AsyncStorage.getItem('userToken');
    //                 alert(AuthToken);
    //             }else{
    //                 alert('Email or Password incorrect')
    //             }

    //         })
            
    //     }catch(err) {
    //         console.log('error')
    //     }
    // }
    handleLogin = async()=>{
        const {email, password} = this.state
        const {navigation} = this.props
        this.props.loading(true)
        try {
         const res = await axios.post(API_LOGIN, {
          email,
          password
         })
       
         if(res !== null){
          const status = res.status
          const data =res.data
          this.setState({token:data.data.access_token})
          await AsyncStorage.setItem('userToken',data.data.access_token)
        //   await AsyncStorage.setItem('userName', data.data.name)
        //   await AsyncStorage.setItem('userImage', data.data.image_url)
          navigation.navigate('Home')
            if(status == 200){
                this.props.loading(false)
                navigation.navigate('Home')
            }else{
                this.props.loading(false)
            }
          const AuthToken = await AsyncStorage.getItem('userToken')
          console.log(AuthToken)
          this.props.login(AuthToken)
         }else{
          Alert("Email or password incorrect")
         }
        } catch(e) {
         console.log("fail post API_LOGIN ",e)
        }
       }

    render(){
        const { navigation } = this.props;
        return(
            <LinearGradient colors={gradient} style={styles.gradient}>
            <Logo />
            <View style={styles.container}>
                <TextInput style={styles.inputBox} 
                    placeholder='Email'
                    keyboardType='email-address'
                    // onSubmitEditing={() => this.password.focus()}
                    underlineColorAndroid='#ffffff'
                    placeholderTextColor = "#ffffff"
                    selectionColor="#fff"
                    onChangeText={val => this.onChangeText('email', val) }
                />
                <TextInput styles={styles.inputBox} 
                    placeholder='Password'
                    secureTextEntry={true}
                    // ref={(input) => this.password = input}
                    underlineColorAndroid='#ffffff'
                    placeholderTextColor = "#ffffff"
                    selectionColor="#fff"
                    onChangeText={val => this.onChangeText('password', val)}
                />
                <Text style={styles.forgot}>Forgot your password?</Text>
                <TouchableOpacity style={styles.button} onPress={this.handleLogin} >
                    <Text style={styles.signupButton}>Login</Text>
                </TouchableOpacity>
                <View style={styles.signupTextCont}>
                <Text style={styles.signupText}>Dont have an account yet?</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Register')}><Text style={styles.signupButton}> Signup</Text></TouchableOpacity>
                </View>
            </View>
            <ThirdLogin />
            </LinearGradient>
        )
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        loading: state => dispatch(handleLoading(state)),
        login: token => dispatch(handleLogin(token))
    }
}

export default connect(null, mapDispatchToProps)(function(props){
    const navigation  = useNavigation();
    return <Login {...props} navigation={navigation} />
})

const gradient = ['#2F80ED', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    container: {
        flex: 1.5,
    },
    logo: {
        marginVertical: 10,
    },
    gradient: {
        flex: 1,
    },
    inputBox: {
        fontFamily: 'Roboto',
        fontSize: 18,
        lineHeight: 21,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'left',
        color: '#F2F2F2',
        marginVertical: 10,
    },
    forgot: {
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: "bold",
        fontSize: 15,
        lineHeight: 18,
        display: 'flex',
        alignItems: 'center',
        textAlign: "right",
        marginVertical: 10,
        color: '#F2F2F2',
    },
    button: {
        width: 150,
        height: 45,
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 18,
        lineHeight: 21,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        backgroundColor: '#0078D7',
        color: '#F2F2F2',
        alignSelf: 'center',
        //borderRadius: 100,
    },
    signupButton: {
        color: '#F2F2F2'
    },
    signupText: {
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontSize: 15,
        lineHeight: 18,
        display: 'flex',
        alignItems: 'center',
        textAlign: "right",
        color: '#F2F2F2',
    },
    signupButton: {
        fontWeight: 'bold',
        color: '#f2f2f2'
    },
    signupTextCont: {
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'row'
    }

})
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './Login';
import SignUp from './SignUp';
import HomeLogin from './index';
import Home from '../main screen/Home';
import MyDrawer from '../Drawer';


const Stack = createStackNavigator();

const Routes = () => {
    return(
        <Stack.Navigator key='root' headerMode='none' initialRouteName='Splash' >
            <Stack.Screen key='splashScreen' name='Splash' component={HomeLogin} />
            <Stack.Screen key='login' name='Login' component={Login} />
            <Stack.Screen key='signup' name='Register' component={SignUp} />
            <Stack.Screen name='Home' component={MyDrawer} />
        </Stack.Navigator>
    )
}

export default Routes;
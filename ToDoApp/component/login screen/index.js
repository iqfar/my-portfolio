import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Logo from './Logo';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { handleLogin } from '../Redux/Action/index';
import { connect } from 'react-redux';
import { set } from 'react-native-reanimated';

class HomeLogin extends Component {
    constructor(props){
        super(props)
    }

    render(){
        const { navigation } = this.props
        return(
            <View style={styles.container} on >
                <LinearGradient colors={gradient} style={styles.gradient}>
                    <Logo />
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        margin: 10
                    }}>
                        <Text style={{
                            fontSize: 21,
                            color: '#f2f2f2',
                            fontFamily: 'Robotica'

                        }}>Welcome To MustDo App</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        alignItems: 'center'
                    }}>
                        <Button title='Masuk' buttonStyle={styles.button} onPress={() => navigation.navigate('Login')} />
                        <Button title='Daftar' buttonStyle={styles.button} onPress={() => navigation.navigate('Register')} />
                    </View>
                </LinearGradient>
            </View>
        )
    }
}

const gradient = ['#2F80ED', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    gradient: {
        flex: 3,
    },

    container: {
        flex: 1,
    },
    button: {
        width: 150,
        borderRadius: 25,
        margin: 10
    }
})

export default (function(props){
    const navigation  = useNavigation();
    return <HomeLogin {...props} navigation={navigation} />
})

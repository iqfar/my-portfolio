import React, { Component } from 'react';
import AppNavigator from './AppNavigator';
import Drawer from './Drawer';
import Profile from './main screen/Profile';
import SignUp from './login screen/SignUp';
import TasksAdd from './main screen/tasks/TasksAdd';
import Home from './main screen/Home';
import HomeScreen from './main screen';
import { Provider, connect } from 'react-redux';
import { store } from './Redux/store';
import { Image } from 'react-native-elements';
import Complete from './main screen/Complete';
import Spinner from 'react-native-loading-spinner-overlay';



class App extends Component{
  render(){
    return(
      <Provider store={store} >
        <AppNavigator />
        {/* <Image source={{ uri: 'https://ik.imagekit.io/kbr5u7uyrf/misaka-mikoto-toaru-kagaku-no-railgun_BZnRg2-UGD.jpg' }} /> */}
      {/* <Complete /> */}
      {/* <Profile /> */}
       {/* <SignUp /> */}
      {/* <TasksAdd /> */}
      {/* <HomeScreen /> */}
      </Provider>
    )
  }
}

export default App
// /*This is an example of Image Picker in React Native*/
// import React from 'react';
// import { StyleSheet, Text, View, Button, Image } from 'react-native';
// import ImagePicker from 'react-native-image-picker';
// export default class App extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       filePath: {},
//     };
//   }
//   chooseFile = () => {
//     const options = {
//       title: 'Select Image',
//       // customButtons: [
//       //   { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
//       // ],
//       storageOptions: {
//         skipBackup: true,
//         path: 'images',
//       },
//     };
//     ImagePicker.showImagePicker(options, response => {
//       console.log('Response = ', response);

//       if (response.didCancel) {
//         console.log('User cancelled image picker');
//       } else if (response.error) {
//         console.log('ImagePicker Error: ', response.error);
//       } else if (response.customButton) {
//         console.log('User tapped custom button: ', response.customButton);
//         alert(response.customButton);
//       } else {
//         const source = response;
//         // You can also display the image using data:
//         // let source = { uri: 'data:image/jpeg;base64,' + response.data };
//         this.setState({
//           filePath: source,
//         });
//         console.log(response.uri)
//       }
//     });
//   };
//   render() {
//     return (
//       <View style={styles.container}>
//         <View style={styles.container}>
//           {/*<Image 
//           source={{ uri: this.state.filePath.path}} 
//           style={{width: 100, height: 100}} />*/}
//           <Image
//             source={{
//               uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
//             }}
//             style={{ width: 100, height: 100 }}
//           />
//           <Image
//             source={{ uri: this.state.filePath.uri }}
//             style={{ width: 250, height: 250 }}
//           />
//           <Text style={{ alignItems: 'center' }}>
//             {this.state.filePath.uri}
//           </Text>
//           <Button title="Choose File" onPress={this.chooseFile.bind(this)} />
//         </View>
//       </View>
//     );
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
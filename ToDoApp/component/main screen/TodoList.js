import React, { Component } from 'react';
import axios from 'axios';
import TodoBar from './TodoBar';
import { AUTH_TOKEN, API_TASKS } from '../API';
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux';
import { save_data } from '../Redux/Action/index';

class TodoList extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: [],
            isComplete: false,
            isImportant: false,
        }
    }

    getData = async () => {
        const AuthToken = await AsyncStorage.getItem('userToken')
        try{
            const res =  await axios.get(API_TASKS, {
                headers: {
                    'Authorization': AuthToken
                }
            })
            if(res !== null){
                const data = res.data.data
                this.setState({ data: data })
                this.props.save_data(data)
                const isImportant = res.data.data.important
                const isComplete = res.data.data.completed
                if(isComplete == true){
                    this.setState({ isComplete: true })
                }else if(isImportant == true){
                    this.setState({ isImportant: true })
                }else if(isComplete == false){
                    this.setState({ isComplete: false })
                }else if(isImportant == false){
                    this.setState({ isImportant: false })
                }
            }
        }catch(err){
            console.log(err)
        }
        
        // axios.get(API_TASKS)
        // .then( res => {
        //     const data = res.data
        //     this.setState({
        //         data: data,
        //     })
        // })
    }

    componentDidMount(){
        this.getData()
        console.log(this.props.data)
    }

    componentDidUpdate(){
        this.getData()
    }

    render(){
        return(
            <TodoBar  data={this.state.data} important={this.state.isImportant} complete={this.state.isComplete} markComplete={this.markComplete} delete={this.delete} edit={this.edit} />
        )
    }
}

const mapStatToProps = (state) => ({
    token: state.auth.token,
    data: state.auth.data,
})  

export default connect(mapStatToProps, {save_data})(TodoList)
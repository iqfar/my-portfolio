import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './Home';
import Profile from './Profile';
import TasksAdd from './tasks/TasksAdd';
import MyDrawer from '../Drawer';
import Complete from './Complete';
import Important from './Important';

const Stack = createStackNavigator();

const HomeScreen = () => {
    return(
        <Stack.Navigator headerMode='none'>
            <Stack.Screen name='Home' component={Home} />
            <Stack.Screen name='Routes' component={MyDrawer} />
            <Stack.Screen name='Profile' component={Profile} />
            <Stack.Screen name='Add Tasks' component={TasksAdd} />
            <Stack.Screen name='Completed Tasks' component={Complete} />
            <Stack.Screen name='Important Tasks' component={Important} />
        </Stack.Navigator>
    )
}

export default HomeScreen;
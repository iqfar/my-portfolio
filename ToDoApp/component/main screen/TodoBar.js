import React, { Component } from 'react';
import axios from 'axios';
import TodoItem from './TodoItem';

class TodoBar extends Component {

    render(){
        // return this.props.data.map((d) =>(
        //     <TodoItem key={d.id} d = {d} markComplete={this.props.markComplete} delete={this.props.delete} edit={this.props.edit}/>
        // ))
        return this.props.data.map((data) => (
            <TodoItem key={data.id} data = {data} markComplete={this.props.markComplete} delete={this.props.delete} important={this.props.important} complete={this.props.complete} edit={this.props.edit} />
        ))
    }
}

export default TodoBar;
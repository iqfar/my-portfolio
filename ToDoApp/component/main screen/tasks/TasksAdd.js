import React, { Component, useState } from 'react'
import { Text, View, StyleSheet, Platform } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { Header, Icon, Button } from 'react-native-elements';
import LeftComponent from './LeftComponent';
import RightComponent from './RightComponent';
import TasksCategory from './TasksCategory';
import { TextInput, TouchableWithoutFeedback, TouchableHighlight } from 'react-native-gesture-handler';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import Axios from 'axios';
import { AUTH_TOKEN, API_TASKS } from '../../API';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { handleLoading } from '../../Redux/Action';
 


const TasksAdd = () =>{
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [deadline, setDeadline] = useState('');
    const navigation = useNavigation();
    const [name, setName] = useState('');
    const [description, setDescrip] = useState('');
    const dispatch = useDispatch();
    // const AuthToken = useSelector(state => state.auth.token)
    
    

    // const getData = async() =>{
    //     try{
    //         const AuthToken = await AsyncStorage.getItem('userToken')
    //         if(AuthToken !== null){
    //             setState({ token: AuthToken})
    //         }
    //     }catch(err){
    //         console.log(err);
    //     }

    //     }
    
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'android' ? false : true);
        setDate(currentDate);
        setDeadline(moment(currentDate).format('DD MM YYYY hh:mm'));
    }

    const onChangeText = (val) =>{
        setName(val)
    }

    const onChangeDescrip = (val) => {
        setDescrip(val)
    }

    const addTasks = async () => {
        dispatch(handleLoading(true))
        const AuthToken = await AsyncStorage.getItem('userToken')

        // try{
        //     await Axios.post(API_TASKS, {
        //         headers: {
        //             'Authorization' : AuthToken
        //         },
        //         name,
        //         description,
        //         deadline,
        //     })
        //     .then(res => {
        //         const status = res.status;
        //         if(status == 201){
        //             alert('Task Added Succesfully')
        //         }else{
        //             alert('Failed to add task')
        //         }
        //     })
        // }catch(err){
        //     console.log(err)
        // }
        try{
        // dispatch(handleLoading(true))
        await Axios.post(API_TASKS, {
            name: name,
            description: description,
            deadline: deadline,
        },
        {
            headers: {
                'Authorization': AuthToken
            }
        }
        )
        .then(res =>{
            if(res.status == 201){
                dispatch(handleLoading(false))
                alert('Task Added Succesfuly!')
                navigation.navigate('Home')
            }else{
                dispatch(handleLoading(false))
                alert('Tasks Add Failed')
            }
        })
    }catch(err){
        console.log(err)
        dispatch(handleLoading(false))
    }
        console.log(AuthToken)
        console.log(name)
        console.log(description)
        console.log(deadline)
    }

    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    }

    const showDatePicker = () =>{
        showMode('date')

    }

    const showTimePicker = () => {
        showMode('time')

    }

    // const dateText = () => {
    //     if(picked !== true){
    //         return <Text>Choose Date</Text>
    //     }else{
    //         return <Text>{date.toDateString()}</Text>
    //     }
    // }

    // const timeText = () =>{
    //     let text;
    //     if(picked !== true){
    //         text = ( <Text>Choose Time</Text> )
    //     }
    //     else{
    //         text = ( <Text>{date.toTimeString()}</Text> )
    //     }
    // }

        return (
            <View style={styles.container}>
                <LinearGradient style={styles.gradient} colors={gradient}>
                    <Header 
                    containerStyle={{ alignItems: 'center', paddingTop: 0, }}
                    leftComponent={<LeftComponent cancel={() => navigation.goBack()} />}
                    rightComponent={<RightComponent add={addTasks} />}
                    centerComponent={<Text style={styles.headerText}>Add Task</Text>}
                    />
                    <View style={styles.category}>
                        <TouchableHighlight><TasksCategory name='calendar-blank' >My Day</TasksCategory></TouchableHighlight>
                        <TasksCategory name='briefcase-outline' >Work</TasksCategory>
                        <TasksCategory name='airplane-takeoff' >Travel</TasksCategory>
                        <TasksCategory name='gamepad-variant' >Entertaiment</TasksCategory>
                    </View>
                    <View>
                        <TextInput style={styles.name}
                        placeholder='Task Name'
                        underlineColorAndroid='#ffffff'
                        placeholderTextColor = "#ffffff"
                        selectionColor="#fff"
                        onChangeText={onChangeText}
                        />
                        <TextInput
                        placeholder='Description'
                        underlineColorAndroid='#ffffff'
                        placeholderTextColor = "#ffffff"
                        selectionColor="#fff"
                        onChangeText={onChangeDescrip}
                        />
                        <View style={{ 
                            alignSelf: 'center',
                            marginVertical: 20
                         }}>
                            <Text style={styles.headerText} >Deadline</Text>
                        </View>
                        {/* <TouchableWithoutFeedback onPress={showDatePicker} >
                            <Text style={styles.date} >{date.toDateString(date)}</Text>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={showTimePicker}>
                            <Text style={styles.date} >{date.toTimeString(date)}</Text>   
                        </TouchableWithoutFeedback> */}
                        <Button title={date.toDateString()} buttonStyle={styles.date} titleStyle={styles.buttonTitle} type='clear' onPress={showDatePicker} />
                        <Button title={date.toTimeString()} buttonStyle={styles.date} titleStyle={styles.buttonTitle} type='clear' onPress={showTimePicker} />
                        {show && (
                          <RNDateTimePicker
                          value={date}
                          mode={mode}
                          is24Hour={true}
                          display="default"
                          onChange={onChange}
                          neutralButtonLabel="clear"
                        />  
                        )}
                    </View>
                </LinearGradient>
            </View>
        )
    }

export default TasksAdd

const gradient = ['#2F80ED', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    gradient: {
        flex: 1,
    },
    headerText: {
        fontFamily: 'Philosopher',
        fontWeight: 'bold',
        fontSize: 25,
        alignItems: 'center', 
        color: '#F2F2F2'
    },
    category: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(242,242,242, 0.5)'
    },
    date: {
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingVertical: 10,
        marginHorizontal: 5,
    },
    buttonTitle: { 
        color: '#f2f2f2', 
        fontSize: 18,
        fontFamily: 'Robotica' 
    },
    // name: {
    //     backgroundColor: '#fff'
    // }
})
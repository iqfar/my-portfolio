import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import { ScrollView, TouchableWithoutFeedback, FlatList } from 'react-native-gesture-handler';
import { Icon, Header } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import TodoBar from './TodoBar';
import AsyncStorage from '@react-native-community/async-storage'
import API_TASKS from '../API';

class TodoList extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: [],
            isComplete: false,
            isImportant: false,
        }
    }

    getData = async () => {
        const AuthToken = await AsyncStorage.getItem('userToken')
        try{
            const res =  await axios.get(`${API_TASKS}?completed=true`, {
                headers: {
                    'Authorization': AuthToken
                }
            })
            if(res !== null){
                const data = res.data.data
                this.setState({ data: data })
                const isImportant = res.data.data.important
                const isComplete = res.data.data.completed
                if(isComplete == true){
                    this.setState({ isComplete: true })
                }else if(isImportant == true){
                    this.setState({ isImportant: true })
                }else if(isComplete == false){
                    this.setState({ isComplete: false })
                }else if(isImportant == false){
                    this.setState({ isImportant: false })
                }
            }
        }catch(err){
            console.log(err)
        }
        
        // axios.get(API_TASKS)
        // .then( res => {
        //     const data = res.data
        //     this.setState({
        //         data: data,
        //     })
        // })
    }

    componentDidMount(){
        this.getData()
    }

    componentDidUpdate(){
        this.getData()
    }

    render(){
        return(
            <TodoBar  data={this.state.data} important={this.state.isImportant} complete={this.state.isComplete} markComplete={this.markComplete} delete={this.delete} edit={this.edit} />
        )
    }
}


class Complete extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: []
        }
    }
      

    getData = () => {
        axios.get(API_TASKS)
        .then(res => {
            const data = res.data
            this.setState({ data: data })
        })
    }

    #renderItem = () => (
        <TodoList />
    )

    #keyExtractor = (item, index) => item.id;
    
    render(){

        const date = moment().format('l');
        const { navigation } = this.props;

        return(
            <View style={styles.container}>
            <LinearGradient colors={gradient} style={styles.gradient}>
            <Header 
            containerStyle={{ alignItems: 'center', paddingTop: 0, backgroundColor: '#43d8c9'}}
            leftComponent={<View>
                <TouchableWithoutFeedback style={{ marginHorizontal: 10 }} >
                <Icon name='menu' color='#f2f2f2' size={40} style={styles.toggle} onPress={() => navigation.toggleDrawer()}/>
                </TouchableWithoutFeedback>
                </View>}
            rightComponent={
                <View>
                    <TouchableWithoutFeedback style={{ marginHorizontal: 10 }}>
                        <Icon name='plus-circle-outline' type='material-community' color='#f2f2f2' size={40} style={styles.toggle} onPress={() => navigation.navigate('Add Tasks')}/>
                    </TouchableWithoutFeedback> 
                </View>
            }
            centerComponent={<View style={{
                flexDirection: 'column',
                // margin: 10,
                alignContent: 'center'
            }}>
                <Text style={styles.text}>My Day Task</Text>
                <Text style={styles.date}>{date}</Text>
            </View>}
            />
            {/* <TouchableWithoutFeedback style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 20 }} >
            <Icon name='menu' color='#f2f2f2' size={40} style={styles.toggle} onPress={() => navigation.toggleDrawer()}/>
            <Icon name='plus-circle-outline' type='material-community' color='#f2f2f2' size={40} style={styles.toggle} onPress={() => navigation.navigate('Add Tasks')}/>
            </TouchableWithoutFeedback>  */}
                {/* <View style={{
                    flexDirection: 'row',
                    margin: 10,
                    justifyContent: 'space-between'
                }}>
                    <Text style={styles.text}>My Day Task</Text>
                    <Text style={styles.date}>{date}</Text>
                </View> */}
                <View style={{
                    flex: 2
                }}>
                <ScrollView>
                    <TodoList  />
                </ScrollView> 
                </View>
            </LinearGradient>
            </View>
        )
    }
}

export default function(props){
    const navigation  = useNavigation();
    return <Complete {...props} navigation={navigation} />
}

const gradient = ['#2F80ED', 'rgba(0, 0, 0, 0.25)'];

const styles = StyleSheet.create({
    gradient: {
        flex: 2,
    },
    toggle: {
        alignSelf: 'flex-start',
    },
    container: {
        flex: 2,
    },
    text: {
        fontFamily: 'Philosopher',
        fontWeight: 'bold',
        fontSize: 30,
        letterSpacing: 3,
        alignItems: 'center', 
        color: '#F2F2F2',
    },
    date: {
        fontFamily: 'Philosopher',
        fontSize: 18,
        display: 'flex',
        alignSelf: 'center',
        textAlign: 'right',
        color: '#F2F2F2',
    }
})
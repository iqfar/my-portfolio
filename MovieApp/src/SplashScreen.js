import React, { useEffect, useState } from 'react';
import LottieView from 'lottie-react-native';
import { useNavigation } from '@react-navigation/native';

const SplashScreen = () => {
    const navigation = useNavigation();
    const [splash, setSplash] = useState(true); 

    setTimeout(() => {
        setSplash(false)
    }, 4000);
    
  return (
    <LottieView
      source={require('./assets/25194-bouncing-stars.json')}
      autoPlay={true}
      loop={splash}
      onAnimationFinish={() => navigation.navigate('My Tab')}
    />
  );
};

export default SplashScreen
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import MyCardReviewList from './MyCardReviewList';

const MyReview = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>My Review</Text>
      <ScrollView>
        <View style={styles.cardReview}>
          <MyCardReviewList />
        </View>
      </ScrollView>
    </View>
  );
};

export default MyReview;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    backgroundColor: '#000',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    margin: 20,
  },
  buttonStyle: {
    borderRadius: 100,
  },
  button: {
    borderRadius: 100,
    width: 135,
  },
  titleReview: {
    alignItems: 'center',
    margin: 10,
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  title: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 24,
    color: '#fff',
    alignSelf: 'center',
    marginTop: 10,
  },
  cardReview: {
    alignItems: 'center',
    flexDirection: 'column',
    margin: 10,
  },
});

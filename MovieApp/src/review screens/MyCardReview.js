import React from 'react';
import {Card, Image, Icon, Button} from 'react-native-elements';
import {View, StyleSheet, Text} from 'react-native';
import Avatar from '../assets/poster.png';

const MyCardReview = () => {
  return (
    <Card containerStyle={styles.container}>
      <View style={styles.titleReview}>
        <View style={styles.titleLeft}>
          <Image source={Avatar} style={styles.titleImage} />
        </View>
        <View style={styles.titleRight}>
          <Text style={styles.title}>Title (Tahun)</Text>
          <Text style={styles.userCredit}>Reviewed Tanggal</Text>
          <View style={styles.ratting}>
            <Icon
              type="material-community"
              name="star"
              size={20}
              color="rgba(255, 194, 0, 0.98)"
              containerStyle={styles.ratingIcon}
            />
            <Text style={styles.ratingText}>9/10</Text>
          </View>
          <View style={styles.buttonContainer}>
            <Button
              type="clear"
              icon={
                <Icon
                  type="material-community"
                  name="pencil-circle"
                  size={30}
                  color="yellow"
                />
              }
              buttonStyle={styles.button}
            />
            <Button
              type="clear"
              icon={
                <Icon
                  type="material-community"
                  name="delete-circle"
                  size={30}
                  color="yellow"
                />
              }
              buttonStyle={styles.button}
            />
          </View>
        </View>
      </View>
      <View style={styles.commentContainer}>
        <Text style={styles.title}>Great Daze!!!</Text>
        <Text style={styles.comment}>asdasdasdjj</Text>
      </View>
    </Card>
  );
};

export default MyCardReview;

const styles = StyleSheet.create({
  titleReview: {
    flexDirection: 'row',
  },
  creditContainer: {
    flexDirection: 'row',
    marginHorizontal: 5,
  },
  titleContainer: {
    flexDirection: 'row',
  },
  ratting: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  titleImage: {
    width: 80,
    height: 120,
  },
  title: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'justify',
    textAlignVertical: 'top',
    letterSpacing: -0.2,
    color: '#000',
  },
  ratingText: {
    textAlign: 'justify',
    marginHorizontal: 5,
    alignSelf: 'center',
  },
  userCredit: {
    fontFamily: 'Roboto',
    fontSize: 14.5,
    textAlign: 'justify',
    letterSpacing: -0.2,
    color: '#000',
  },
  commentContainer: {
    marginVertical: 10,
  },
  comment: {
    fontFamily: 'Roboto',
    fontSize: 12,
    textAlign: 'justify',
    letterSpacing: -0.2,
    color: '#000',
    marginTop: 7,
  },
  container: {
    width: 325,
    borderRadius: 20,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  titleRight: {
    marginHorizontal: 15,
    marginBottom: 10,
  },
  button: {
    alignSelf: 'flex-start',
  },
});

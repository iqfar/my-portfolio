import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './Home';
import HomeDetail from './HomeDetail';
import CardItemList from './CardItemList';
import HomeGenre from './HomeGenre';
import HomeSearch from './HomeSearch';
import CardItemDetail from './CardItemDetail';

const Stack = createStackNavigator();
const HomeRouter = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home Details"
        component={HomeDetail}
        options={{headerShown: true, headerTitle: 'Movie Detail'}}
      />
      <Stack.Screen
        name="Home Genre"
        component={HomeGenre}
        options={{headerShown: true, headerTitle: 'Movie by Genre'}}
      />
      <Stack.Screen
        name="Card"
        component={CardItemList}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home Search"
        component={HomeSearch}
        options={{headerShown: true, headerTitle: 'Search Movie'}}
      />
      <Stack.Screen
        name="Card Detail"
        component={CardItemDetail}
        options={{headerShown: false, headerTitle: 'Search Movie'}}
      />
    </Stack.Navigator>
  );
};

export default HomeRouter;

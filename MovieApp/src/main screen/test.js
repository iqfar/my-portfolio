import React, { Component } from 'react';
import Axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';
import { View, Text } from 'react-native';

export default class TestFlatList extends Component{
  constructor(props){
    super(props);
    this.state = {
      data: []
    }
  }

  componentDidMount(){
    try {
      Axios.get('https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/movies/1')
      .then(res => {
        const data = res.data
        console.log(data)
        this.setState({ data: data.data.movies })
      })
    } catch (error) {
      console.log(error, 'error')
    }
  }

  render(){
    console.log(this.state.data)
    return(
      <FlatList 
      data={this.state.data}
      renderItem={({item, index}) => {
        <View key={item.id}>
          <Text>{item.title} title</Text>
        </View>
      }}
      />
    )
  }
}
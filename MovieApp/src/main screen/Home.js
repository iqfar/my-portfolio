import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {SearchBar, Icon, Button} from 'react-native-elements';
import CardItemList from './CardItemList';
import CardItem from './CardItem';
import {ScrollView, FlatList} from 'react-native-gesture-handler';
import Axios from 'axios';
import {useDispatch} from 'react-redux';
import {GENRE, GENRE_NAME, SEARCH} from '../Redux/Reducer/case';
import CardItemListGenre from './CardItemListGenre';
import {useNavigation} from '@react-navigation/native';

const Home = () => {
  const [search, setSearch] = useState('');
  const [genres, setGenres] = useState([]);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const onChangeSearch = val => {
    setSearch(val);
  };

  useEffect(() => {
    getGenre();
  }, []);

  const getGenre = async () => {
    try {
      const res = await Axios.get(
        ' https://api.themoviedb.org/3/genre/movie/list?api_key=621cc88a43841ecdeeccc92b8bd84cca&language=en-US',
      );
      if (res !== null) {
        const dataGenres = res.data.genres;
        setGenres(dataGenres);
      } else {
        console.log('error');
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };

  const genreChange = (id, name) => {
    dispatch({type: GENRE, payload: id});
    dispatch({type: GENRE_NAME, payload: name});
    navigation.navigate('Home Genre');
  };

  const renderButton = ({item, index}) => (
    <Button
      buttonStyle={styles.genreListButton}
      icon={
        <Icon
          type="material-community"
          name="movie"
          color="#040303"
          size={20}
          containerStyle={styles.genreListButtonContainer}
        />
      }
      title={item.name}
      titleStyle={styles.genreListButtonTitle}
      onPress={() => genreChange(item.id, item.name)}
    />
  );

  const handleSearch = title => {
    dispatch({type: SEARCH, payload: title});
    navigation.navigate('Home Search');
  };

  return (
    <View style={styles.container}>
      <SearchBar
        placeholder="Type here ...."
        searchIcon={
          <Icon
            type="material-community"
            name="magnify"
            size={20}
            color="#979797"
          />
        }
        value={search}
        onChangeText={onChangeSearch}
        inputContainerStyle={styles.searchContainer}
        containerStyle={styles.searchBarContainer}
        onSubmitEditing={() => handleSearch(search)}
      />
      <View style={styles.titleContainer}>
        <Text style={styles.titleGenre}>Best Genre</Text>
        <Text style={styles.genreDetail}>more >></Text>
      </View>
      <View style={styles.genreListContainer}>
        <FlatList
          horizontal
          data={genres}
          renderItem={renderButton}
          keyExtractor={item => item.id}
        />
      </View>
      <View>
        <Text style={styles.genreListTitle}>Hot Movie</Text>
      </View>
      <ScrollView>
        <View>
          <CardItemList />
          {/* <CardItem /> */}
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    backgroundColor: '#040303',
  },
  searchContainer: {
    backgroundColor: '#F6F7F7',
    borderRadius: 10,
  },
  searchBarContainer: {
    backgroundColor: '#040303',
    margin: 10,
  },
  titleContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    margin: 10,
  },
  titleGenre: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 20,
    letterSpacing: -0.2,
    color: '#F6F7F7',
  },
  genreDetail: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 12,
    letterSpacing: -0.2,
    color: '#F6F7F7',
    alignSelf: 'flex-end',
  },
  genreListContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 10,
  },
  genreListButton: {
    backgroundColor: '#F6F7F7',
    marginHorizontal: 5,
    borderRadius: 6,
  },
  genreListButtonTitle: {
    color: '#040303',
  },
  genreListButtonContainer: {
    marginRight: 5,
  },
  genreListTitle: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 20,
    letterSpacing: -0.2,
    marginHorizontal: 20,
    marginVertical: 5,
    color: '#F6F7F7',
  },
});

import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Routes from './login screen/Router';
import Spinner from 'react-native-loading-spinner-overlay';
import {useSelector, useDispatch} from 'react-redux';
import {createStackNavigator} from '@react-navigation/stack';
import MyStackRoot from './MyRootStack';
import ModalRate from './ModalRate';
import ModalEditRate from './ModalEditRate';
import AsyncStorage from '@react-native-community/async-storage';
import {ISLOGIN, SUCCESS} from './Redux/Reducer/case';
import LottieView from 'lottie-react-native';
import Animated, {Easing} from 'react-native-reanimated';
import AwesomeAlert from 'react-native-awesome-alerts';
import Alert from './Alert';

const Stack = createStackNavigator();

const AppNavigator = () => {
  const isLoading = useSelector(state => state.auth.isLoading);
  const dispatch = useDispatch();
  const progress = new Animated.Value(0);

  const getData = () => {
    try {
      AsyncStorage.getItem('userToken').then(token => {
        if (token) {
          dispatch({type: SUCCESS});
          console.log(token);
          dispatch({type: ISLOGIN});
        } else {
          dispatch({type: SUCCESS});
        }
      });
    } catch (error) {
      console.log('no token');
      dispatch({type: SUCCESS});
    }
  };

  useEffect(() => {
    getData();
  }, [getData]);

  return (
    <NavigationContainer>
      <Spinner visible={isLoading} size="large" />
      {/* <Alert /> */}
      <ModalRate />
      <ModalEditRate />
      <MyStackRoot />
    </NavigationContainer>
  );
};
export default AppNavigator;

/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Logo from './Logo';
import {Button} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';

const HomeLogin = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container} on>
      <Logo />
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          margin: 10,
        }}>
        <Text
          style={styles.titleStyle}>
          Welcome To Movie Review App
        </Text>
        <Text style={styles.titleStyle}>Login or Register to Review</Text>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
        }}>
        <Button
          title="Masuk"
          titleStyle={styles.titleButton}
          buttonStyle={styles.button}
          onPress={() => navigation.navigate('Login')}
        />
        <Button
          title="Daftar"
          titleStyle={styles.titleButton}
          buttonStyle={styles.button}
          onPress={() => navigation.navigate('Register')}
        />
      </View>
    </View>
  );
};

export default HomeLogin;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#040303',
  },
  button: {
    width: 150,
    borderRadius: 25,
    margin: 10,
    backgroundColor: '#FEA800',
    color: '#040303',
  },
  titleButton: {
    color: '#040303',
  },
  titleStyle: {
    fontSize: 21,
            color: '#F6F7F7',
            fontFamily: 'Robotica',
  }
});

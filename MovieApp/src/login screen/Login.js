import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {Text} from 'react-native';
import {Button} from 'react-native-elements';
import ThirdLogin from './ThirdLogin';
import Logo from './Logo';
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import {API_LOGIN} from '../API';
import AsyncStorage from '@react-native-community/async-storage';
import {useDispatch} from 'react-redux';
import {PROCCESS, SUCCESS, FAILED, ISLOGIN} from '../Redux/Reducer/case';
import MyTab from '../MyTab';

const Login = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();

  const onChangeEmail = val => {
    setEmail(val);
  };

  const onChangePassword = val => {
    setPassword(val);
  };

  const handleLogin = async () => {
    dispatch({type: PROCCESS});
    try {
      const res = await Axios.post(API_LOGIN, {
        email: email,
        password: password,
      });

      if (res !== null) {
        const status = res.status;
        const data = res.data;
        if (status === 201) {
          await AsyncStorage.setItem('userToken', data.data.token);
          navigation.navigate('Home')
          dispatch({type: SUCCESS});
          dispatch({ type: ISLOGIN })
        } else {
          // eslint-disable-next-line no-alert
          dispatch({type: FAILED});
          alert('Email or Password Invalid');
        }
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: FAILED});
    }
    console.log(email);
    console.log(password);
  };

  return (
    <View style={styles.container}>
      <Logo />
      <TextInput
        style={styles.inputBox}
        placeholder="Email"
        keyboardType="email-address"
        underlineColorAndroid="#B4B4B0"
        placeholderTextColor="#B4B4B0"
        selectionColor="#B4B4B0"
        onChangeText={onChangeEmail}
      />
      <TextInput
        style={styles.inputBox}
        placeholder="Password"
        secureTextEntry={true}
        underlineColorAndroid="#B4B4B0"
        placeholderTextColor="#B4B4B0"
        selectionColor="#B4B4B0"
        onChangeText={onChangePassword}
      />
      <Text style={styles.forgot}>Forgot your password?</Text>
      <Button
        title="Login"
        buttonStyle={styles.button}
        titleStyle={styles.signupButtonTitle}
        onPress={handleLogin}
      />
      <View style={styles.signupTextCont}>
        <Text style={styles.signupText}>Dont have an account yet?</Text>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate('Register')}>
          <Text style={styles.signupButton}>Sign up</Text>
        </TouchableWithoutFeedback>
      </View>
      <ThirdLogin />
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#040303',
  },
  inputBox: {
    fontFamily: 'Roboto',
    fontSize: 18,
    lineHeight: 21,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'left',
    color: '#F6F7F7',
    margin: 10,
  },
  forgot: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 18,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'right',
    margin: 10,
    color: '#F6F7F7',
  },
  button: {
    margin: 5,
    alignSelf: 'center',
    borderRadius: 100,
    width: 124,
    backgroundColor: '#FEA800',
  },
  signupText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontSize: 15,
    lineHeight: 18,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'right',
    color: '#F6F7F7',
    marginHorizontal: 5,
  },
  signupButton: {
    fontWeight: 'bold',
    color: '#F6F7F7',
  },
  signupTextCont: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row',
  },
  signupButtonTitle: {
    color: '#040303',
  },
});

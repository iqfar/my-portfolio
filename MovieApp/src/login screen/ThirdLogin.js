import React, {Component} from 'react';
import {View, Image, Text, ImageBackground, StyleSheet} from 'react-native';

const IconLogin = props => {
  return (
    <View style={styles.logo}>
      <Text style={styles.text}>{props.children}</Text>
    </View>
  );
};

export default class ThirdLogin extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.slice}>
          <Text style={styles.line} />
          <Text style={styles.sliceText}>or</Text>
          <Text style={styles.line} />
        </View>
        <View style={styles.logoLogin}>
          <IconLogin>F</IconLogin>
          <IconLogin>G+</IconLogin>
          <IconLogin>In</IconLogin>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    position: 'absolute',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 24,
    // lineHeight: 28,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#F2F2F2',
  },
  slice: {
    display: 'flex',
    flexDirection: 'row',
    marginVertical: 10,
    justifyContent: 'center',
  },

  logo: {
    borderColor: '#F2F2F2',
    borderRadius: 100,
    borderWidth: 3,
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 15,
  },

  logoLogin: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 10,
  },
  container: {
    flex: 1,
  },

  sliceText: {
    fontFamily: 'Roboto',
    fontSize: 18,
    lineHeight: 21,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    top: 10,
    marginHorizontal: 10,
    color: '#F2F2F2',
  },
  line: {
    borderColor: '#F2F2F2',
    borderBottomWidth: 3,
    width: 150,
    top: 1,
  },
});

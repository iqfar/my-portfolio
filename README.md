# My Portfolio

this is my Portfolio history about learning a Javascript Programming, Mostly About React Native.
in this portfolio you can see my progress in learning Javascript for past 3 months.
i will update if i had another project in future.

list project (from beginning to now)
1. Todo Web using open source API and Reactjs
2. Todo App using API and React Native with CRUD and Login system
3. Movie Review App using API, React Native and Redux
4. Task Manager for Scrum Method like Trello using API, React Native, Redux and Redux-Thunk

before using the source you must install it first
example:

```yarn install```
or 
```npm install```

then running it use npm or yarn
example;

for Web using
```yarn start``` or ```npm start```

for App using
```yarn start``` or ```react-native start```
then
```react-native run-android```

note: i will just show my own training project, for the company or bussiness project i will just display the link to the products and not the entire source code